# DoubleGDP Handbook

Welcome to the DoubleGDP Handbook repository.

To view the handbook visit https://handbook.doublegdp.com/handbook/

For information on how to contribute to it or create a local copy visit https://handbook.doublegdp.com/handbook/setup/
