---
title: User Guide Overview
---


## Purpose
Welcome to the DoubleGDP App user guide. This guide provides step-by-step instructions to city administrators and city residents on how to get the best from the app.

