## Map

The Map helps the user to navigate the city and pinpoint exactly where the city is located in a broader aspect. View the [Map Feature](https://app.doublegdp.com/map).

1. The Map can be viewed from a range of modes such as;
<ul>
<li>OSM</li>
<li>Satellite</li>
<li>Mapbox</li>
</ul>

2. The User is able to select and identify different ares on the map by selecting; 
<ul>
<li>Land Parcels - When the User clicks on this checkbox the map brings up the Legend of 'plots sold' and 'unkown.' The User iis able to identify which plots are sold and those that are not yet sold.</li>
<li>Points of Interest - When the User clicks on this checkbox it brings up the areas marked by the blue/white pin. Click on the pin of interest and it will bring up the information about that particular area.</li>
<li>Sub-urban Areas - When the user clicks on this checkbox it highlights the divided suburb areas of the city.</li>
<li>Coverage Area</li>
</ul>

