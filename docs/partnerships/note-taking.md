_This page of the handbook outlines Partnership's method of chronicling important information about the people and organizations we meet with._

## Partnership Call Process
When you have conversation with an external party on behalf of DoubleGDP, please do the following:
1. Share that you're having a Partnerships conversation in slack in the #partners-and-marketing channel.
2. Check if the external party is already listed in our CRM [Airtable](https://airtable.com/tblI8yl7CDFKZc1D2/viwfj84vTQPR3P3O1?blocks=hide). If they are not, please add them. If they already are, ensure that you have the relevant context of our previous discussions by checking the notes document here: [Call Notes with Potential Partners](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit#heading=h.htey0d28qty8).
3. Follow the note-taking guidelines below.  



### Note-taking
 We create a new tab and record notes or link to a note document here: [Call Notes with Potential Partners](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit#heading=h.htey0d28qty8). This document serves as our single source of truth to record all partnerships conversations. It is shared with all@doublegdp.com

For prospective partners, or contacts of DoubleGDP with recurring conversations, we list the partnership conversation in the [Call Notes with Potential Partners](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit#heading=h.htey0d28qty8) document along with a reference to one or two documents:

1. [Mandatory]: A document shared with all@doublegdp.com and our prospective partner where we record agenda items and high level notes with action items.
1. [Optional]: An internal document shared with all@doublegdp.com where we share research learnings and discuss the partnership.

Once a prospective partner reaches the Opportunity stage or later, we ensure that all notes documents are also linked to an Account plan in a Gitlab epic.  



### Our CRM

[Airtable](https://airtable.com/tblI8yl7CDFKZc1D2/viwfj84vTQPR3P3O1?blocks=hide) serves as our Customer Relationship Manager 'CRM'. In it we track information about potential cities, contacts, and articles & reports.
