# Partnerships
As described in our [team charter](https://handbook.doublegdp.com/company/teams/), Partnerships is responsible to bring in new partners who will utilize our platform (as it is or as we can envision it) for their users. We can be reached at partnerships@doublegdp.com.


## Market

"Partners" include cities / property developers / community organizations, and their users include staff, residents, businesses, and prospective members of their communities.

The ideal DoubleGDP partner in 2021 is a city development with significant 18-month population potential led by a pragmatic entrepreneur with an established real estate track record.

 - Population potential: >10,000
 - 2021 plans: grow population from .5% to 5% of potential


## Market Insights

Here are the customer insights that we are learning about our market: [Customer Insights](https://docs.google.com/document/d/1NFHF1v3iHJyG8KHVvOEbjFGihQmYx0dnHmk0ceMCaCI/edit#heading=h.5mzg90e0f5op)


## Partnership Relationship Development "Funnel"

Our relationships with our partners also develop and mature over time.

In our [internal CRM](https://airtable.com/tblI8yl7CDFKZc1D2/viwfj84vTQPR3P3O1?blocks=hide), we also show the maturity of how those relationships are developing. These relationships go through the following stages:

1. **Prospect:** Identified multi-use real estate project with substantial population potential and is likely to have residents in 2021 or 2022
1. **Lead:** Booked an exploratory meeting with a prospect
1. **Qualified Lead:** Close likelihood >10%: Established interest in DoubleGDP platform, and identified several modules for use/co-development.
1. **Opportunity:** Close likelihood >40%: Aligned on a vision for a partnership. Further developed fit, and established mutual interest in moving forward to outline a service agreement. Quantified estimated revenue and 12-month population
1. **Negotiation:** Close likelihood >70%: Drafting or negotiating a service agreement
1. **Active Partner:** Agreement signed to start building software for new partner

![image](/img/partnerships/funnel2.png)

### Pipeline stages and definitions

This is an alternative definition of our funnel stages. Needs to be merged with the above.

- `No status / nurture` - we've not had contact, but are aware of their existence
- `Prospect` - we've had contact or reached out. no active conversation.
- `Lead` - we've spoken, but not yet sure it's a qualified opportunity; e.g. either org may have some criteria that are unknown or that the other org does not meet
- `Qualified Lead` - we have reason to believe that both orgs meet each other's basic requirements
- `Opportunity` - we've established mutual interest in working together
- `Negotiating` - we've received a verbal intent to partner; working on terms
- `Active partner` - we've started working together

## New City Development Maturity "Funnel"

We've learned that partners have different needs depending on the maturity of the development at the new cities and new communities they are building, supporting or investing in.

In our [internal CRM](https://airtable.com/tblI8yl7CDFKZc1D2/viwMB0Y3OS97CDzgE?blocks=hide), we show a landscape of new city and community development projects that we are aware of.

These cities typically go through the following very simplified development lifecycle, which we use to categorize opportunities for partnership:

**Planning and Fundraising** -> **Sales and Infrastructure Development** -> **Pioneers Building** -> **Active Community - Pioneers** -> **Active Community - Early Adopters** -> **Active Community - Mainstream**
