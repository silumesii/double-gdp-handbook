## Hello World!
I am Bonny from Nairobi, Kenya.

### About
I was raised in a small town called [Nanyuki](https://en.wikipedia.org/wiki/Nanyuki) that I would highly recommend to visit. You'll see a spectacular view of Mt. Kenya especially in the evening when the ice caps are sunlit.

### Interests
Those are always changing but reading, football and travelling have been consistently things I love doing. I also love photography and whisky.
