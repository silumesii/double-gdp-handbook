# Minza's hello world

I love the outdoors and am a frequent hiker and traveler. I grew up in Karachi, a city with a population of 14.91 million as of 2017, so nature always had an allure and an inviting charm. As a kid, my escape from city life involved driving to the beach or going on road trips to nearby parks with my family.

I feel lucky that I now live in San Francisco, where urban life intermingles fluidly with nature, and where I can go on long walks (and sometimes short bike rides) to work off the dessert that I had for one of my three meals. Did someone say pastries?!

I often spend time in LA and love it there too.
