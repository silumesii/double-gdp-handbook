---
title: Intro
---
# Hello Worlds

We want to build a culture of "[document first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/)" communications. In order to do this, every teammate must learn how to contribute to the handbook. To facilitate this, we have a "hello world" directory and require that every new teammate use it to make their first contribution to the handbook.

We ask that each teammate add one page that is reflective of something about them or something that they care about and add it into this directory. You may make it personal or not, share your name or remain anonymous. Just say "hello world" or put a list of your favorite bands, but do something that is yours.  Note that the handbook is public, so keep in mind that anything you put here will be available on the internet. Also, DGDP teammates can see who makes what change to the handbook, so they will know what you write even if you do not explicitly identify yourself.

Here are the steps to contribute:

1. Create a new page in the hello_world and name it something unique. I suggest your first name, but it's not required. It does need to end with `.md`. So for example, Nolan's page is [`nolan.md`](nolan/).
1. Add some content to your page. Try adding a heading, some text, and a list -- these are the basics you'll need to know. You may also want to try adding a hyperlink.
1. Commit your change. See the guidelines for commit messages in the [Handbook Setup Guide](/handbook)
1. Wait 2 minutes, and then confirm your change on our public handbook [Hello World](https://handbook.doublegdp.com/handbook/hello_world) section
