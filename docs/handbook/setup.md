---
title: Handbook Setup
---

## Turn On Notifications

In order to make use of the Handbook it's important that you are able to receive notifications from GitLab. Here are instructions on how to configure this:

1. Open `Settings` from your profile drop-down at the top right of [GitLab](https://gitlab.com)
1. Select `Notifications` from the `User Settings` menu on the left nav bar
1. Confirm that the `Notification email` has your @doublegdp.com email address
1. Set the `Global notification level` to `Participate`
1. Confirm that all of the `Groups` and `Projects` on the lower section of the page are set to `Global`

This will configure GitLab to send a notification email on all tasks and changes for which you are a participant or @mentioned, and it should be your default setting. You may modify these settings to better suit your preferences (instructions are available in [GitLab documentation](https://docs.gitlab.com/ee/user/profile/notifications.html)), but it is your responsibility to stay aware of requests made of you through GitLab.

*Screenshot illustrating the proper notification settings...*
![GitLab Notification Settings](../img/handbook-notifications.png)


## Set up a local copy
You can contribute entirely through the online process above. However, you may also wish to set up a local copy of the handbook. This will make it easier to preview your changes and also gives you access to a few other writing tools that make it more user friendly. There's a bit more complexity to this process, but this guide should get you going.

To set up a local version of this handbook, you will need at least the following set up on your machine:

1. Git -- see these [installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
1. A Markdown editor -- we recommend [Typora](https://typora.io/) for a simple visual experience or [Atom](https://atom.io/) if you want an environment integrated with git.
1. A local copy of the repository (instructions below)

This setup will be sufficient to make changes to the documentation, and is user-friendly for non-developers. You'll be able to see and contribute the files within the documentation, but won't have the same UI as on the website. If you want that full experience, you please install [MkDocs](https://www.mkdocs.org/).


### Setting up Git
You will need to set your credentials in git. This may be a bit daunting at first, so we'll step through it together. Instructions are [here](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

<details>
<summary>Git setup for Mac</summary>

<p>From your terminal, run the following:</p>
<ol>
<li> <code>git config --global user.name "John Doe"</code></li>
<li> <code> git config --global user.email "johndoe@doublegdp.com"</code></li>
</ol>
</details>



### Local copy of repo
To create a remote copy of the repository, open your terminal interface and navigate do the directory where you want to store it. (Nolan suggests `~/Projects`.) Then run `git clone https://gitlab.com/doublegdp/handbook.git`; this will create a `handbook` directory that has the latest copy of the project.



## Make your first edit
To make your first edit:

1. In your terminal, run `git pull`
1. Open your markdown editor from the `docs/` directory within the Handbook
1. In your markdown editor, make a change and save it
1. In your terminal run `git add .` or `git add docs/name_of_changed_file.md `
1. In your terminal, run `git commit -m "Make first contribution"`[^commit]
1. In your terminal, run `git push`


## Run locally

If you want to run the handbook locally so you can see edits before committing, you also will need to set up your local environment to run mkdocs and other dependencies for the handbook. Setting up a local environment sometimes is easy and we'll outline the simple steps here. Unfortunately, complexities can sometimes arise based on other things in your environment; if this happens try Googling the error message and see if you can easily resolve it. If not, reach out for help!

The basics that you'll need are:

1. Some installer -- we recommend [Homebrew](https://brew.sh/)
1. [MkDocs](https://www.mkdocs.org/) -- try `brew install mkdocs`
1. [Mkdocs Material](https://squidfunk.github.io/mkdocs-material/) theme -- try `pip install mkdocs-material`
1. [Material Extensions](https://pypi.org/project/mkdocs-material-extensions/) -- try `pip install mkdocs-material-extensions`

Note that in order to do this, you will need to know a bit about how to use the command line within the `Terminal` app (assuming you're using a Mac). Here's a [quick primer](https://www.theodinproject.com/paths/foundations/courses/foundations/lessons/command-line-basics-web-development-101).








[^commit]: This creates a 'commit' with a message. A `commit` is an individual change to a set of files. Your message should be a brief and concise description of a simple change you're making to the docs. Make the first word of your commit message a verb, and use present tense.
