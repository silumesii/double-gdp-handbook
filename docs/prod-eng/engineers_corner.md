# Engineer's Corner

## General Meeting Etiquette

 - **Always have the video on.**
   - The only exception is when the connection is bad
 - be on time.
 - Always read the agenda ahead of time.
 - Always have the agenda open during the meeting.
 - Help take notes in the agenda.

## End of day Sync

The agenda for the end of sync meeting is here: [https://docs.google.com/document/d/1KgCQd25Ce_R3L5gwRlDHynf8CJhV3Oj7LxBiR0WsUP0/edit](https://docs.google.com/document/d/1KgCQd25Ce_R3L5gwRlDHynf8CJhV3Oj7LxBiR0WsUP0/edit)

  - Write in the time based agenda what you did today, and what you are going to work on tomorrow.
  - If you need to discuss a topic with the team, write the topic or question in the agenda.
  - This meeting is not optional and you must attend.
  - The end of day sync will be used primarly for removing blockers.



## 1-1 Meeting Format
Everyone in engineering have a weekly 1-1 with their direct supervisor to discuss whatever topics are on their mind.  And each meeting must have an agenda which follows the rolling format outline here: [https://handbook.doublegdp.com/company/meetings/#rolling-agenda](https://handbook.doublegdp.com/company/meetings/#rolling-agenda)

  - Add topics and discussions items at least 10 minutes before the meeting (engineer and direct manager).

  - Please have a section of 'What's on your mind?'

  - If neither (engineer and direct manager) have nothing to discuss, then 1-1 is cancelled for that week.


## Guidelines for Interviewing Engineers

I am a DoubleGDP Engineer and I am interviewing an engineer like me, What do I do?

- **Come prepared**

  - Read the candidate 's resume with care and note the areas you like and the areas where you want clarification.

  - Depending on the tech stack, **review the questions list** for the appropriate stack to get you in thinking about insightful questions you want to ask the candidate.   The questions list are just a guide to help you.  You are not required to used it (although everyone does.)

    - General ReactJS. Questions list that we can ask engineers during an interview can be found [here](https://docs.google.com/document/d/19qaK11bz2hdTbHIdDLFPkjN8XmlAvi6J2CmxfQGEdiY/edit#heading=h.ybbld1bahdwl) --  Probably shamelessly compiled from other web sites.

    - General Rails (*Coming Soon*)

  - If the candidate has been given a **Coding Challenge**, you must

    - **Complete the coding Challenge** yourself and that you understand it completely.   
    - Install the candidate's coding challenge on your environment, make sure **the project runs** and **study the code** so that you can ask questions.  
      - How was the install process documented?
      - Was the code clean?  Did the code went through a Linter?
      - Was the code readable ?
      - Was there Unit tests?   What about Code coverage?
      - Compare your findings with the seniority of the position.

  - You can find the coding challenges here:

    - [For Rails](https://docs.google.com/document/d/1qJY10sceHmqtKHs5Coc6apDurWUXaTsV8m7A0j9BtOU)
    - [For ReactJS](https://docs.google.com/document/d/1WtqgYuKnYAM8qVeK-DOe_LXvOAzm6zD5Hm0YbP8kTok)

- After The Interview

  - Go to Greenhouse.io
  - Fill out the evaluation form.
  - Discuss your observations with the hiring manager , but no one else until everyone in the interviewer group have completed their evaluations in Greenhouse.
