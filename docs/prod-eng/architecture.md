# Architecture And Design
At DoubleGDP, we use ADR to communicate Architecture Concepts and Decisions.   Not everything is an ADR and they usually contains changing design concepts that will affect the team .
https://resources.sei.cmu.edu/asset_files/Presentation/2017_017_001_497746.pdf

## Design Guidelines

### General
 - All designs must have a mobile view and a desktop view(if applicable).
 - All interfaces must be consistent.
 - All pages with lists must include pagination with a limit of 50 items per page.  
 - Everything should be shareable via a link(tabs state, modal state, pagination state).  
 - All mutations with side effects must have a loading state to show a user that an action is taking place.  

### Input form Guidelines
 - When building a form soliciting user input (ie: collecting transaction information and such), required fields should be marked with an asterisk.   
 - Before submitting the form to the backend, a check for all required form must be perfomed and errors reported on a field by field basis. 
    -  ![input_error_handling](../img/engineering/architecture_error_handling_single.png){: style="height:75px;width:150px"}
 - successfully submitting a form must result in a green snackbar with the successful operation message
    - Reference: [SnackBar](https://material-ui.com/components/snackbars/#customized-snackbars)
 - When a submitted form results in an error, a red snackbar must be presented with a LEGIBLE message that anyone can understand.
 - during the submission process, a progress indicator must be presented for the duration of the operation.
    - Reference: [Progress indicator](https://material-ui.com/components/progress/) 

    
## Cybersource Implementation
```mermaid
sequenceDiagram
    participant Client
    participant DoubleGDP
    participant CyberSource
    participant DGDP Customer Merchant Account
    participant Client CC Account
    Client-->>DoubleGDP: Client enters CC Info
    DoubleGDP->>CyberSource: Forward the CC info with the merchant Account
    CyberSource ->> Client CC Account: Request funds
    Client CC Account ->> CyberSource: Sends funds
    CyberSource ->> DGDP Customer Merchant Account: Deposits fund 
    DGDP Customer Merchant Account ->> CyberSource: Returns an acknowledgment
    Note right of CyberSource: No funds are ever deposited <br/>in DoubleGDP's Account
    CyberSource ->> DoubleGDP: Returns an acknowledgment
    loop DBTransaction
        DoubleGDP->>DoubleGDP: Record transaction
    end
    DoubleGDP ->> Client: Returns acknowledgment
```

## Currency handling

To make sure that amount of money is well formatted and follows community currency we use the [Intl.NumberFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat).
- Always make sure the currency is handled per community
- Remember to use the `formatMoney(currencyData, amount)` function everywhere we are rendering amount of money in the application
currencyData is an object that contains currency(ZMW) and locale(en-ZM)

## Error Codes From GraphQL
List of Error codes returned by GraphQL when an issue occurs:

| Error Code | Description |
| ------ | ------ |
|   |   |


## ADR
Template for ADR can be found at:
https://gitlab.com/doublegdp/app/-/wikis/Architectural-Decision-Records
ADR are currently kept in the gitlab wiki. 
To submit an ADR: 
 - Create a new wiki page and paste the Markdown from the link above
 - Fill in the requested information and sent an email / slack to the team for review / comments / discussions/ brainstorming.
