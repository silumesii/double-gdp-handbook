# Story Complexity

## Background

Estimating the complexity of a story is a constant struggle because each team member have a slightly different skillset so each member vote based on what they know.  Often, estimations end up varying widly.  In addition, the exercise is repeated everytime a story need to be evaluated, which is often time during backlog refinement and/or sprint planning.   



## Complexity Weight

We weigh our complexity with the number of days we think a story will take to finish:

| Gitlab Weight | Label  | Description                                              |
| ------------- | ------ | -------------------------------------------------------- |
| 1             | Low    | 5 hours of work                                          |
| 2             | Medium | 9 hours work                                             |
| 3             | High   | 18 Hours worth of work                                   |
| Too big       | X-High | > 18 hours of work -  Story is too big - Must be smaller |



## Complexity Map

The complexity map is a set of predefined attributes associating the story with its complexity.   This is a work in progress, but it is has helped engineers find consensus among themselves.   

| Low                                                          | Medium                                                       | High                                                         | X-High |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------ |
| Add a card on the screen                                     | feature that needs some work on both back end and front end  | Work that requires updating a feature in production that is high risk and/or visibility. | TODO   |
| Adding a link or routing to a button                         | Needs some changes done to the logic of the app (back end)   | Requires external api’s/Requires third-party integration/and external integration | TODO   |
| Cosmetic changes(ie: Update to fonts and icons)              | Working on a full small feature with a minimum of one day effort and half a day of testing. | Involves db migrations                                       | TODO   |
| Changes happen in 1-2 files in the code base.                | Affects more than 3 ruby files (test files not included)     | Requires change of mutations or table structure              | TODO   |
| An independent task that takes less than 4 hours to implement and test | Affects multiple screens                                     | Includes change of current architecture of a specific existing feature. | TODO   |
|                                                              | Affects more than 6 react files                              | Has multiple screens and Has front end and backend work      | TODO   |
|                                                              |                                                              | Has data security implications (third party APIs accessing DGDP data). | TODO   |
|                                                              |                                                              |                                                              | TODO   |
|                                                              |                                                              |                                                              |        |

### Gitlab Voting

In the story itself, developers do vote by adding a **numbered** reaction Emoji to the story:

![image-20200722182025702](/img/gitlab-reaction.png)



| To Vote:       | Use                                                   | retrieving emoji                              |
| -------------- | ----------------------------------------------------- | --------------------------------------------- |
| 1              | ![image-20200722185410847](/img/gitlab-emo1.png)      | Type:one, then scroll at the end of the list. |
| 2              | ![image-20200722185020079](/img/gitlab-emo2.png)      | Type: two                                     |
| 3              | ![image-20200722185459845](/img/gitlab-emo3.png)      | Type: three                                   |
| Xtra humongous | ![image-20200722185554022](/img/gitlab-whalesize.png) | type: whale                                   |



After voting, the average of the results is entered in the 'weight' field in gitlab.



