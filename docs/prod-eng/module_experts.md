# Module Experts - Role Playing

## Engineering Subject Matter Experts

Team,
The following list is the list of experts for whenever you:

- need advice on the application
- need to looping an engineering expert in a discussion
- have a question about how something works.

Responsible experts are with (\*).
Here are the modules and experts :

- User management
    - Saurabh
    - **Nurudeen** (\*)
- Gate Access
    - **Olivier** (\*)
- Construction mgt & Time sheets
    - **Olivier** (\*)
- Community Engagement (Discussions, News, Social Media)
    - **Olivier** (\*)
- Messages/Notifications
    - **Saurabh** (\*)
    - tolu
- Campaigns
    - **Nurudeen** (\*)
- Forms and Workflows
    - Olivier
    - **Saurabh** (\*)
    - Victor (workflows)
- Payment/Invoices
    - Olivier
    - **Saurabh** (\*)
    - Tolu
- Land Management
    - **Victor** (\*)
- Devops
    - **Nurudeen** (\*)
    - Victor
- Product Design and User Interface Design
    - **Gbemi** (\*)

## Engineering Role Playing

Each engineer is required to perform a role of as a user in their selected user types at least 3 times in a week.  
Following is the list of roles and engineers responsible to perform that role.

- Admin
    - Everyone
- Client
    - Okeugo
    - Gbemi
- Contractor
    - Nurudeen
- Custodian
    - Nurudeen
- Prospective Client
    - Tolulope Olaniyan
- Resident
    - Saurabh Shinde
- Security Guard
    - Olivier
- Visitor
    - Olivier

### Role Playing Checklist

These are checks an engineer should make sure that they are passing when performing role play, these checks assume a community that supports all features that we currently have in the app.

**Client**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their past payments
- Is able to be scanned at the gate
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access their own plot(s)

**Contractor**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their own timesheet
- Is able to be scanned for time recording and for gate access
- Is able to access their own profile
- Should not have access to search, payments, etc ...

**Custodian**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see timesheet for all employees
- Is able to be scan or search for other users
- Is able to record shifts for Security guards and Contractors
- Is able to access their own profile
- Is able to access contractor and security guard's profiles
- Should not have payments, etc ...

**Prospective Client**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access active community discussions
- Is able to access community businesses

**Resident**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their past payments
- Is able to be scanned at the gate
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access their own plot(s)

**Security Guard**

- Is able to Login with:
    - Facebook
    - Google
  - Phone Number
- Is able to see their own timesheet
- Is able to be scan or search for other users
- Is able to manually record entries at the gate.
- Is able to access their own profile
- Is able to access other users's profiles
  - Profile should only show restricted contact information for user verification

**Visitor**

- Is able to Login with:
    - Facebook
    - Google
    - del> Phone Number
- Is able to see their own profile
- Is able to access community news
- Is able to access community support
