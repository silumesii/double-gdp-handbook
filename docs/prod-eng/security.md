# Product Architecture and Security

Our product architecture overview and security guiding principles are [publicly shared.](https://docs.google.com/presentation/d/1LTFfLLNcau3f00CHMSClBLMnLAP8T64fPhsZsMhrCmE/edit?usp=sharing)


### Security Overview

#### Development

1. Production data is kept separate from development data and it doesn't leave production servers
2. DoubleGDP does not develop on real customer data and follows the client’s guidelines on data access
3. We encrypt communications (SSH, HTTPS) to development servers.


#### Production

1. We encrypt communications (SSH, HTTPS encryption) to our production servers.

   1. End-to-End encryption through CloudFlare.

2. We encrypt files at rest using S3 server side encryption.  SSE-KMS. 
   https://docs.aws.amazon.com/AmazonS3/latest/dev/serv-side-encryption.html

3. Heroku postgrees encryption: 
   https://devcenter.heroku.com/articles/heroku-postgres-production-tier-technical-characterization#data-encryption (Database is encrypted at the block-level storage encryption)

   
#### Sensitive File Handling

 1.  Engineers are sometimes given access to files containing sensitive information through google docs and following Google encryption procedures:
     https://services.google.com/fh/files/misc/google-workspace-encryption-wp.pdf

 2.  Files are access using HTTPS

#### Vulnerabilities.
 1. As part of our deployment process, we monitor for vulnerabilities and address them promptly.

#### Engineer Training on Privacy

   Starting January 2021, Engineers will be required to attend data privacy training.  
