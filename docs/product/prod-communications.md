---
title: Product Communications
---


## Product Status and Updates

Product sprint updates are provided every two weeks along with the rest of the company. Product Management's [sprint update presentation](https://docs.google.com/presentation/d/1iVlDBLg_zCipZUwMRbj4k6MZUD5-42Q7n54sMxUH4KY/edit#slide=id.gbaec4e76d8_0_13) is available publicly. 

The product sprint update includes the following:

- High product priority areas for current sprint
- High product priority areas for upcoming sprint
- Product Manager's focus for current and upcoming sprint
- Metrics or stats to show adoption and use in product priority areas





### Product Release Notes

We  distribute a written summary of new features at the end of each sprint. The aim is to make this easy to create and distribute -- approximately one phrase or sentence per feature released in the sprint. Here is the process for creating this output:

1. Engineering populates the "Release Notes" field on each push to production
1. At the end of the sprint, PM curates that list and format for external consumption [due Wed at noon PT -- alongside the PM sprint video]
1. CEO leverages that list as input to his [Sprint Update Slides](https://docs.google.com/presentation/d/1wrCcckL8qgNKSjWaHtbj0KS-F0A77YCuxJf5-0mwVdI/edit)
1. PM distributes the notes to customers as part of the [Sprint Updates](https://docs.google.com/presentation/d/1iVlDBLg_zCipZUwMRbj4k6MZUD5-42Q7n54sMxUH4KY/edit#slide=id.gc67f73e8ff_0_0)
