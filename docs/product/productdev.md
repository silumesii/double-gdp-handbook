# Product Management

Product documentation is located in a [gDrive folder](https://drive.google.com/drive/u/1/folders/1oHUmTsiw3GV_vrILXbuMKLDWBEY-0U-o).

[discuss] What of these documents are useful and worth maintaining moving forward?



## Design Principles

Our Principles to move forward with new features expeditiously are grounded in our overall CREDIT values specifically on Results, Efficiency, and Iteration:

1. Focus on small steps that deliver value
    1. Great to have a line of sight to bigger vision, but non-blocking
    2. Root designs in existing application and workflows
2. Aim for “executable” proposals -- a well-formed design should be able to be accepted or rejected as is
    3. Until it can move forward as is, it should be refined or simplified to reach this bar.
3. Solicit input on changes to existing workflows
    4. Mutale should identify dependencies, especially training for guards and admins, and stay coordinated with proposals
4. Honor or explicitly renegotiate customer agreements
    5. Product should document any agreements or promises to customers
    6. Design should be able to propose changes, and either product or CS can advise on the ability to renegotiate or push forward and address with training
5. Take a modular approach to product design and architecture
    7. Wherever possible the product should have a modular design and architecture to provide flexibility of product and accelerate the development process

### Design System
In order to ensure consistency and convenience while working on our product, we have an evolving [design system](https://sketch.cloud/s/aea0fb6f-9cf5-4f1b-8185-e05679493188) of reusable components guided by our standards that we assemble to build our application.


## Product Planning Process

The product management process aims to create and promote transparency between our customers and teams. It is structured to give visibility into our overall roadmap, upcoming sprint, and current sprint activity. Both our Development and non-Development teams work on sprint cycles and GitLab  is used to track our progress.


### GitLab Project Board Flows

Below is an overview of our Gitlab projects and how we use them to manage our product design and development cycles.


#### Feature suggestions

Here’s how to add a new idea:

- Before adding a feature, perform a quick search in the Platform project to make sure it was not previously suggested to avoid duplicates. If it has been previously suggested, add your input and ideas as a comment to the task
- If the feature was not previously suggested, then create a new issue in the Platform project, select the feature dropdown in the field, fill out all the fields
- During our product planning discussions, we will categorize the suggestion and follow up with any questions. Where appropriate, we’ll group it with other features requests that have similar components
- The originator of the issue will be tagged as a stakeholder on the ticket and will be responsible for providing additional information and reviewing the ticket when on staging before being promoted to production
- If it is a high priority feature request, then leave a comment for the Product Manager in the task with @[name]



#### Product Roadmap

The product roadmap provides us visibility into our broader product vision and how our iterations and sprints build to larger features through Epics.

The roadmap can be found here: https://gitlab.com/groups/doublegdp/-/roadmap

- Epics or larger features are categorized under 3+ month, 2 months, or 1 month
- Stories within these Epics are moved to Active Planning when the team begins scoping the story for future sprint
- Once a task is ready to be considered for an upcoming sprint, it is moved to one of the upcoming milestones of the sprint planning board. (Found here: https://gitlab.com/doublegdp/app/-/boards/1844746)
- Tags are used extensively in stories for reporting and tracking. Frequently used tags include "Sprint Priority" for all stories that are active in the current sprint, "Spike" for research stories, "Bug" to mark 'Please Fix me' issues.

#### UI/UX Design Sprint

UI/UX Design works at least 1 sprint ahead of the Dev Sprint.   By the time engineers estimates story during sprint planning, all the stories that should have designs and flows will have designs and flows.

#### Dev Sprint

Development and non-development stories that are active in the current sprint are captured in Dev Sprint.

- Dev Sprint Project is organized by Open, To do, Doing, Staging,Closed. Stories that have to be prioritized at the sprint start are moved into Open column.
- Dev Sprint can only have 5 stories per engineer, unless:
   - All the stories are completed and there s time left in the sprint.
   - All the stories have a complexity of 1.  (never happens)
   - A Priority 0 bug need to be worked on.
   - leftover tickets from the previous sprint already started.
- When a Dev Sprint has started, the only way to add a ticket in the sprint is to take one out.





### Product Planning Meetings

Goal: A cycle of product roadmap and brainstorm meetings are planned every Monday, Wednesday, and Friday to keep product planning at least 1 sprint ahead of the development cycle. The ultimate goal is for product planning to be 2 sprint cycles ahead but given the changing nature of client needs, we recognize this may evolve based on the business and urgency of other requests.

|        | Monday                                        | Tuesday                                   | Wednesday                    | Thursday                     | Friday             |
| ------ | --------------------------------------------- | ----------------------------------------- | ---------------------------- | ---------------------------- | ------------------ |
| week 1 | decide sprint experiments                 |                                           | Retrospective/Sprint Kickoff |                              | eng/prod check in. |
| week 2 | Reviewed user stories with technical feedback | Async Sprint Planning Review Announcement |                              | Synchronous  Sprint Planning |                    |
| week 3 | decide sprint experiments                 |                                           | Retrospective/Sprint Kickoff |                              |                    |

Guidelines for us to hold each other accountable:

- These meetings are focused on planning for the following sprints. Topics focused on the current development sprint should only be addressed if there is time remaining or if it is an urgent issue. Any urgent items are be noted at the start of the meeting so the agenda can be adapted accordingly.
- Each meeting is focused on sharing information, seeking input for a decision, or making a decision and the goals are provided at the start of the meeting.
- All sessions are recorded for the convenience of our broader team and to maximize asynchronous time.


## Sprint Planning Process

Our sprints are designed to facilitate asynchronous remote work across multiple time zones. They establish a regular cadence of releasing features to customers and the internal synchronous communications required to plan and coordinate.

While sprints are our primary product development process, we also have regular company and other meeting cadences that help us stay coordinated.


### Sprint Retrospective and Planning meetings

Goal: Sprint retrospective and planning meetings take place on Thursday every 14 days. The first part of the meeting is reserved for a sprint retrospective and is used to provide the team key takeaways for improving sprints. The second part of the meeting is focused on sprint planning and marks the close of the previous sprint and the start of the next sprint. This time is also used to define the goals for the sprint, align on sprint stories, and move stories based on priorities and capacity.

#### Sprint Planning

Our Sprint Planning has 2 parts.  

##### Asynchronous Sprint planning

Because we are an all remote team, it is rather difficult to find a good time for all of us to meet.  In addition, meetings can be boring (especially sprint planning meetings).  Engineers must have all the following sprint stories graded before the SP meeting.  No email required. Thus, when the product manager signals the stories for the following sprint are ready for **Async Sprint Planning Review**, the engineers must:

- review the issues in the upcoming sprint and independently vote https://handbook.doublegdp.com/prod-eng/story_complexity/
- review the completeness of the stories.  If the story is missing keys components such as acceptance criteria, the engineer should bring it to the product manager's attention by adding a 'thumb down' and an explanation of what's missing in a *comment*.
- ensure they understand the story.   If they do not understand the story they should bring it to the product manager's attention by adding a 'thumb down' and an explanation of what's missing in a *comment*.  


##### Synchronous Sprint Planning

The meeting is really to answer questions that have been raised by engineering during **Asynchronous Sprint planning** .

Guidelines for us to hold each other accountable:

- Sprint planning and retrospective meetings are designed to make decisions on moving stories to plan or to deprioritize. Any design or solutioning discussions should take place before sprint start during Product Planning meetings. A go/no-go decision will be made on the story based on the story’s complexity and size. If there are too many unknowns for a story it should be deprioritized for a later iteration.
- The meetings have a packed agenda. We should keep each other accountable on agenda and time and make quick decisions or provide concise feedback to keep the meeting moving. We should remind each other to move items to other meetings if they need further discussion
- Besides the moderator, there are rotating roles for the meeting to assist with timekeeping, task creation, and capacity tracking with the following responsibilities: Time Keeper remind team of how much time is left for each agenda item, Task Assigner creates tasks that come up during the meeting in Asana, Capacity Tracker keeps a count of Low, Med, High stories.



### Sprint Demo

We finish our sprint with a recording that is posted on youtube.  The audience for these updates is the rest of the team at DGDP, the broader community, and our investor.  We also hold a meeting whose purpose is to communicate the features developed by the engineering team during the sprint, answer any related questions and concerns, and gather feedback from all the departments.  

Suggested format of information to cover in these updates is:

1. What did you aim to accomplish this sprint?
2. What progress did you make against those goals?
3. What were highlights and lowlights of the experience?
4. What are your goals for the next sprint?
5. Anything else of note -- upcoming events, thank yous, time off, something fun?

#### When
 - The Sprint end Tuesday (already does).
 - The demo Q&A will be on Thursday.
 - meeting length to 30 mins

#### Who is it for
 - Everyone who has questions about the engineering sprint Videos.
 - The meeting would be for answering questions and discussions.
 - meeting is optional.

#### How
- All demo videos are due by Tuesday 8PM CAT (This provides enough time for anyone to watch at their own speed, except those in PT)
- Everyone watches the videos ahead of time
- Function Heads can have up to 5 mins.
- Videos must be kept under 2 mins/person.
    - All Engineering videos must be demoed **in mobile mode** and **not desktop mode**.
    - Each story is demoed by an engineer who has worked on the issue.  Each presenter must give credit to all collaborators on the issue.  **Each presenter must record a 1 - 2 minutes video showcasing the stories have worked on during the sprint.  Each video must be uploaded to the youtube DoubleGDP channel [YouTube channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig) at least 12 hours prior demo. The link is then shared in the #general Slack channel.
    - Design will also post their video on Youtube

**How to upload video to DoubleGDP channel**

- You need to have "Manager" permission to the DoubleGDP YouTube channel granted to you.
- Once this is granted, navigate to youtube.com. Make sure you are logged in to your DoubleGDP account
- Click the account settings icon on the top right corner
- Click on the "switch account" menu and select DoubleGDP
- Upload the video by clicking the video camera icon and following the instructions
- Add the video to the playlist titled "Progress Updates YYYY-MM-DD". Use YouTube's `Save to Playlist` feature, and create the playlist if it does not already exist.
- Share the video to #general channel on Slack

#### Prerequisite
 - Everyone should add 2 out of 3 of the following items at least 8 hours before the following to the [demo doc](https://docs.google.com/document/d/1_4MRX2H4l9ajG5d9NK1OrOOUsqcMlFqtRaZJ153pSQ0/edit) prior to the meeting:
   - Most useful feature.
   - one question
   - What's missing?
      - Out of all the features that have been developed, is there a feature that would improve the experience dramatically?

CEO will watch team videos, record a summary, create a playlist from them, and post to social media. CEO Updates are due by 9:30p PT.

### Engineering Retrospective

The meeting agenda has 3 parts:
- 'What we liked about the sprint' (10 mins)
- 'What we did not liked about the sprint'  (10 mins)
- 'What can we improve' (30 mins).
  - Goal is to find 1 actionable item to improve the next sprint

Engineers must fill in their responses at least 1 hour before the retrospective meeting [link here](https://docs.google.com/document/d/1hvIiCohHwWwnNJPqAq1VfqrQBvzF-ZT3A0f-FbGHvH0/edit#)
