---
title: Product Vision
---

## Product Vision

We are building an end-to-end platform for new cities to connect with residents, accelerate growth, and deliver responsive public services. DoubleGDP's [Product Vision](/company/product-vision/) provides an overview of how we see our platform working with our city clients to deliver on their goals.


## Platform Terms of Use

The following “Terms of Use” govern the use of the DoubleGDP, LLC website and platform, including all content or information hosted therein and any subdomains and services thereof. By accessing or using the DoubleGDP website and platform, you agree to be bound by our Terms of Use and by our Privacy Policy. For any questions or concerns regarding our Terms of Use, please contact us at [support@doublegdp.com](mailto:support@doublegdp.com).

<iframe src="https://docs.google.com/document/d/e/2PACX-1vQfiUzI44PucBSGAeZ_BQ8pFI5ETkrEU5yMZ9dubyekeGQMdbQsReXfruIAh48FMjYPPLlwFJtquzgL/pub?embedded=true"style="width:100%; height:300px;"></iframe>


## Platform Privacy Policy

We are committed to protecting your personal information and your right to privacy. Our privacy policy highlights what information we collect, how we use it and what rights you have in relation to it. If you have any questions or concerns about our notice, or our practices with regards to your personal information, please contact us at [legal@doublegdp.com](mailto:legal@doublegdp.com).

<iframe src="https://docs.google.com/document/d/e/2PACX-1vQnzZGUOWtQx1YygjFlQT24jL4on11s8Ksh2aZz8ILE_x4PgGhygOIuMVK_rHL1oVSKRqBN009rqnAJ/pub?embedded=true"style="width:100%; height:300px;"></iframe>



## User Data Deletion and Copy

To request the deletion of your account and user data or to receive a copy of your data for personal records, please contact us at [support@doublegdp.com](mailto:support@doublegdp.com) with the following information:

- Full Name
- Email
- Phone Number
- Place of residence (Country, State, City)
- Type of Request: Data Deletion or Data Copy
- Any other pertinent information to identify account

Your name, email, and phone number is used to identify your account, while your place of residence is used to identify the rules that regulate your data.

Once we receive your request, a support representative will reach out to you promptly to get any additional information to process your request. After the request is processed, you will receive an email to confirm your data has been deleted or a copy has been provided for your records.


