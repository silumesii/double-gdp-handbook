# Productivity Tools

We use a variety of productivity tools in addition to those mentioned in [Communications](#communications). You’re welcome to use your own private productivity tools. Here’s a list of those that the company licenses for shared productivity or for which we have company accounts:

| **Application** | **Purpose** |
| --- | --- |
| Airtable | Knowledge base of articles and company contacts |
| Bill.com | Manage invoices and accounts payable |
| Brex | Company credit card |
| Calendly | Calendar scheduling |
| Coursera | Learning and development |
| CultureAmp | Performance Review Tool |
| Digital Ocean   | [engineering] Remote dev environment   |
| Docker   | [engineering] Containerization of packages for deployment |
| Earth Class Mail | Manage postal mail online
| Expensify   |  Submit expenses to finance   |
| Facebook   | Social media: <a href="https://www.facebook.com/doublegdp/">https://www.facebook.com/doublegdp/</a>   |
| Gitlab   | Source control and website hosting    |
| Google Analytics   | Website traffic analytics   |
| [Google Cloud](https://console.google.com) | Dev tools |
| [Google Data Studio](https://datastudio.google.com/) | Reporting |
| Google G Suite   |  Email and office applications   |
| [Google Marketing Platform](https://marketingplatform.google.com/home) | Linked org for Analytics and Data Studio |
| Google BigQuery | Data replication via Google Cloud |
| Greenhouse   | Applicant Tracking System and job board   |
| Gusto    |  Payroll for US-based teammates |
| Heroku   | [engineering] Cloud application hosting |
| Hexnode   | Runs our app in kiosk mode for security guards’ phones   |
| Hootsuite | Posts to and monitors a variety of social media channels |
| Invision    | [still in use?]   |
| LinkedIn   | Social media: <a href="https://www.linkedin.com/company/doublegdp">https://www.linkedin.com/company/doublegdp</a>  |
| [Loom](https://loom.com) | Record and share videos easily |
| Nexmo   | [engineering] Send SMS messages from app   |
| OnePassword   | Store shared passwords and account information    |
| PagerDuty  | On call schedule to receive app error / outage notifications. Trigger a notification through +1-802-990-0911 |
| [Phrase](https://phrase.com) | Application translation and localization |
| QuickBooks   | [finance] Manage accounting and payments   |
| Ramp   | Company Credit Card for CSM
| Redis   | [engineering] Data store, cache, and messaging   |
| Rollbar  | [engineering] Detect and manage app exceptions   |
| SendGrid | Email messaging support through the app |
| Silicon Valley Bank | [finance] Manage cash and make wire transfers   |
| Sketch | User Interface Design   |
| Slack  | Team communications & chats   |
| [Stitch](https://www.stitchdata.com/) | Connect Heroku with BigQuery databases |
| Storybook  | React Documentation Component https://doublegdp.gitlab.io/app  |
| Twitter | [Marketing] Handles: @nolanmyers and @2xgdp  |
| WhatsApp[^wa] | Commonly used to communicate with people in Zambia   |
| Wise  | International Payments App.  |
| YouTube   | Community updates in [company channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig?view_as=subscriber) |
| Zeplin | UI Design and style guides |
| Zoom  | Video conference calls |


[^wa]: Not licensed, but commonly used for work-related activity
