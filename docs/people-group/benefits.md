
## **Benefits**

In this section, we describe what we offer to our Team Members; our default policy is to provide benefits for all team members:

* US-based team members can select their benefits through [Gusto](http://gusto.com)
* Non-US based team members receive a monthly stipend of **USD 300** to cover for:

    * A premium healthcare plan. (We aim for the second-best available.)
    * Internet fees
    * Cell phone bill
    * Bank fee deductions from your paycheck

You don't need to do anything to receive this; it will be included by default in your monthly paycheck.

If this does not cover your monthly expenses, you may opt to share the details of your expenses, and we will adjust your stipend accordingly. To do this, please share the following information with [peopleops@doublegdp.com](mailto:peopleops@doublegdp.com):

1. Your healthcare bill. We'll calculate the stipend based on 80% for team members and 50% for dependent.
1. Your internet bill
1. Your cell phone bill
1. Any other recurring expenses that are aligned with our policies.

We'll recalculate and adjust your default monthly stipend accordingly.
