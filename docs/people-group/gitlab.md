## GitLab

We use GitLab for many important communications. Processes leveraging it will be documented on their own pages. This page outlines the setup of GitLab itself.

We have four repositories:

1. **Handbook** - the source code for our Handbook. The audience of the Handbook is DoubleGDP Teammates. This is written for us and by us. This covers company processes. Note that issues created in this project are by default public, and can be marked confidential.
1. Platform - our product code. Issues and processes in this repo are by default public, as is our source code.
1. Web - our website.
1. Coding Challenge - to help evaluate engineerng candidates
1. PagerDuty Notifier - to convert SMSs to PagerDuty incidents
1. Certbot - to manage SSL authentication

## Icons
These icons are configured at the top of the projects, and on the Slack incoming webhooks.

- Handbook <img src="/img/icons/dgdp_handbook.png" width="50px" alt="Handbook" />
- Platform <img alt="Platform" src="/img/icons/platform_icon.jpg" width="50px" />
- Website <img alt="Website" src="/img/icons/dgdp_favicon.png" width="50px" />

## GitLab Practices and Etiquette

Much of our day to day correspondence takes place in GitLab. It's worth outlining some of the etiquette and practices we use within this tool:

1. Work that needs to be done and requires collaboration between more than 2 teammates should be created as an `issue` in GitLab. See above for guidance on which Project it should be placed in.
1. Issues may have `todo` items within them. If ambiguous, they should include who owns them and a due date. The "due date" for an individual task is written as text next to the task, not the overall due date for the Issue.
  1. Todos may be split into separate issues if they have many sub-dependencies, if the owner of a sub-dependancy needs to manage the tasks Due Date via reminder in GitLab, or require a separate group of more than 2 teammates to coordinate independently of the main issue.
  1. Todos should be listed in the `Description` rather than nested in comments. When listed in this section, GitLab will summarize the number of checked and still-open todos in the issue status bar.
1. Comments generally should be made in the default `Comment` box at the bottom of the issue. However, if you responding to a specific point in a comment further up in the thread, you can `Reply` to that specific comment from the action buttons in its header.
1. Use the `Assignee` field to list the person or people who have direct responsibility for next steps acting on an issue, and `Participants` for a cc list of people to keep aware.
1. :warning: **Reduce risk of inadvertent mentions in our GitLab projects by adding the correct DoubleGDP Group `Assignee`.** Example: @cecilia11 not ~~@ceciliaeis~~ :warning:

### Using GitLab, GDocs, and Handbook
A rule of thumb for using GitLab issues, Google Docs, and the Handbook

1. Google Docs are useful for active conversations, notes, whiteboarding. Decisions should not captured in these.
1. GitLab issues are to capture decisions, get approval on them, or roll them out to a broader team.
1. The Handbook records the current process. As a decision is made, it should be documented in the handbook via a merge request. (We should consider "making a decision" synonymous with documenting it in the handbook.)

## Epics

We use `Epics` to describe and coordinate work around topics that are broader than an individual issue; this may be program, longer-term, project, set of features, or grouping of related `issues`.

A few notes on their usage:

1. Epics are live at a `Group` level within GitLab, and not within a specific `Project`. This means that conceptually they describe work that we want to do, and allow us to coordinate effort across multiple projects. For instance, achieving one epic may require work in the product, from customer success, and updates in the handbook.
1. Epics may have associated sub-epics or issues.
1. Epics should contain only a `Description` and commentary around refinement of that description. They should not contain work or todos; anything that needs to be done in order to achieve the epic should be tracked as an `issue` instead.

## Approvals

Approvals, for budget or program plans, are a great way to clarify our thinking and ensure strategic alignment when investing substantial time or money. An approval should be written as an `issue`, since it will be closed once complete.

1. Use the issue description to provide a summary of your proposal. It should cover in a paragraph or two: what you intend to do, expected results, total timeline and cost, and why you think this approach makes sense. The summary alone should provide enough context for any DGDP teammate reasonably versed in our strategy and context to quickly understand the intent and make an assessment of whether the approach seems reasonable to them, *without referencing any other materials or documents*.
1. For approvals that seem well-aligned with strategy or previous plans, a single paragraph may be sufficient. For more involved decisions or larger investments, it will be helpful to provide more detail and context, links to other issues or documents, to help the audience understand and contribute to the proposal.
