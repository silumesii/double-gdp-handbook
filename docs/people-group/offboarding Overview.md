## Voluntary Offboarding

A voluntary offboarding occurs when a team member informs his or her manager of a resignation. The choice to leave DoubleGDP was their decision.

If you are a current team member and you are thinking about resigning from DoubleGDP, we encourage you to speak with your manager, or another trusted team member to discuss your reasons for wanting to leave. At DoubleGDP we want to ensure that all issues team members are facing are discussed and resolved before a resignation decision has been made.  This will allow DoubleGDP the ability to address concerns and in return foster a great work environment.

If resignation is the only solution after you have discussed your concerns, please communicate your intention to resign to your manager. We would advise you to review your employment contract for the statutory notice period and determine your last working day. Then with your manager you can discuss the time needed to work on a handover/transition. If there’s no notice period included in your employment contract we would advise that you provide DoubleGDP 2 weeks of notice. Hereafter please review the below process which will be followed for a voluntary resignation. 

### Voluntary Process

1. **Team Member**: Team members are requested to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. **Team Member**: The team member should provide a written resignation letter or email notification to their manager.
1. **Manager**: Create an Offboarding ticket
