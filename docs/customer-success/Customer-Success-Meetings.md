
## Collaboration with DoubleGDP Teams

Customer Success has a few standing meetings:

 - All hands Customer Success sync - every Monday and Wednesday 4PM CAT

 - Customer Success, CEO and Product sync - every Thursday 6PM CAT
 
 - Customer Success, Product, Engineering sync - every Wednesday 6PM CAT


### Customer Success, CEO and Product Sync

There will be one weekly session for Customer Success Team, Product and CEO interactions. The agenda will include:

 - Standard CS Updates 
    - AIR Program
    - HIR Program
    - Population Updates (tracking actual population against target)
    - Construction Pipeline and programs to accelerate construction
    - Housing stock updates

 - Product Updates (led by Head of Product)

 - Open agenda: In this slot any meeting attendee can add topics not included in the 1 and 2 

Updates to the agenda should be added to the "[CS team sync document](https://docs.google.com/document/d/1K6BegZkcpofhgJWV8KZ8IkMw0Y6tPlwkfOaUqpc3PyU/edit#)" by Thursday 2pm CAT to allow for adjustment to the time allocated to each agenda item.

### Customer Success Managers Meetings with Customers

Customer Success Managers (CSM) have frequent engagement with customers. The DoubleGDP meeting guidelines apply. 
