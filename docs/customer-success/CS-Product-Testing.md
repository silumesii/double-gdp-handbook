## Customer Success Product Testing

The customer success team plays a vital role in the deployment and adoption of the DoubleGDP application across partner cities. 

Through this process the team will ensure product knowledge is internally embeded and passed on to DoubleGDP city partners with an aim of helping them reach their growth goals through the app. 

### Overall Responsibilities
Members of the CS team have a responsibility of having an overall appreciation of the product. 

Each member has been assigned product areas of focus. For which they will personally schedule time to understand the product area. They can also connect with the engineer responsible for their product area as detailed in [engineering's module experts](https://handbook.doublegdp.com/prod-eng/module_experts/).

**Assignments of the Product Areas:**

1. Mutale:

 - Payments, Invoices and Collections
 - Maps and Property Registry

2. Penny:

 - Campaign Management System (Sales)
 - Content Management System (News)
 - Community Engagement and Support

3. Zoussi:

 - Gate Access & Emergency Reporting
 - Service Requests and Incident Reporting
 - Task Management System 

4. Doreen: 

 - CRM, Customer Journey and Pipeline
 - Forms, contracts, and workflows

 5. Gabriel

  - Business Registry


### Changes to Product 
DoubleGDP releases several product changes in a sprint. 

The CS team member will take note of the changes in their product area at the end of each sprint as communicated through the engineer sprint updates. They will personally schedule time to test the functionality on the [demo.app](https://demo.doublegdp.com/) and give feedback during the [Thursday retrospective meeting](https://handbook.doublegdp.com/product/productdev/#sprint-demo). 


### User Guide 

The CS team member will keep the [user guide](https://handbook.doublegdp.com/User%20Guide/index.html) up to date with instructions on how to use the functionality and updates to the functionality as they are  released.

**User Guide Update Tips**

1. Consider the audience: An end-user and admininstrator's view are different. The guide should taken into account all user types. 

1. Text or video: Depending on the functionality using a video guide would be better than text or vice versa. Videos should be no longer than 2 minutes while text should be no more than 10 steps. 

1. When text is used, screenshots are required for visiaulisatioin:

    - The screenshot should be taken from the demo or staging environements and not display any Personal Identifiable Information (PII)

    - All screenshots annotations and markings (arrows, text, etc) shall be in orange colour
    
    - One screenshot can be used to demonstrate several steps by using numbering to denote each step. 
