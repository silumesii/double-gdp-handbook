---
title: Customer Success Homepage
---

## Customer Success at DoubleGDP

The Customer Success team works with our customers to help them get value from our platform and services. This includes:


1. Driving adoption of the product through [user trainig](/city-processes/training-the-customer/) and [user guides](User-Guide)
1. Running [programs](/programs/)
1. Designing [city processes](/city-processes/) 


## Sprint Reporting

The CS Team publishes sprint updates to inform teammates and customers stay informed about what we're working on, and invite them to engage further in topics that they are interested in.

The core sprint reporting topics:


1. Population - Work done to influence populatio growth/ Number of residents
1. Housing Stock - Work done to influence the housing stock in the city
1. Product - User Guide edits; User trainings conducted; App feature requests and bugs raised


### Sprint Management

Sprint Planning and Retrospective Review is done every 2nd Monday during the Customer Success Team sync. 

Sprint Retrospective topics include:


1. What was achieved in the previous sprint?
1. What will move to the next sprint or will progress over several sprints?
1. Lessons Learned/ what could be done better 

Sprint Planning topics include: 


1. What is planned for the upcoming sprint?
1. Help required within the sprint

### Documenting the Sprint Update

Slides for the updates are available through GDrive as "[CSM Sprint Update](https://docs.google.com/presentation/d/1Yq1kPRROuXTLs3ZKLNvL8OcPGjGJdzkeS_9Gn44yrwk/edit#slide=id.g9aa6f6f5d2_0_201)"

Each topic mentioned in sprint updates will be linked to a corresponding issue in GitLab. This has a few purposes:

1. Depth of information. We provide summaries in video talking points, but the link provides a reference where the audience can learn more.
1. Engagement. We encourage participation and the link provides an easy entry point for teammates to provide input or help on specific issues that interest them.
1. Communication consistency. This helps us ensure that we're utilizing GitLab for all our processes and leveraging handbook-first evolution.

The sprint update recording is uploaded to [DoubleGDP's YouTube channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig). The video should be added to the sprints playlist: "Progress Updates yyyy-mm-dd".



