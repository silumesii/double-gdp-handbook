# Customer Success Enablement 

Customer success professionals possess a unique set of skills that help our customers achieve thier goals. The skillset ranges from having a deep understanding of the product and uncovering customer painpoints; articulating business needs and city processes; change management and customer experience among others. 

This enablement guide provides mandatory and recommended training for the customer sucess team. 


### Tracking Progress

1. **Survey:** On completing a training topic, the CSM shall complete a [survey](https://forms.gle/KNccATu5aVLqvTeKA). The information will be used to improve the training process and provide reference for CSM's career development. 

2. **Office-Hours Sync:** During the CS sync that falls on every first Wednesday of the month, the team shall review the training progress made and share any learings that would be beneficial to the team. 


## Customer Success Fundamentals

1. **[Active Listening](https://www.linkedin.com/learning/effective-listening/understand-the-big-picture-2):** Customer Success Managers (CSM) are constantly building relationships. Relatioinship management involves actively listening to the customer; understanding their needs and work processes and creating an environment where the customer feelds free to reach out for assistance. In this 1-hour training, the trainers show you how to assess your current listening skills; understand the challenges to effective listening and develop the behaviours that will allow you to become a better listener. 

1. **[Change Management](https://www.linkedin.com/learning/change-management-foundations-10041380/change-management-foundations-course-overview):** In a CSM's world things are always changing. It could be a customer changing its business strategy or the CSM helping a businesses adopt a new product functionalilty. This 46-minute course covers the foundations of change managment and provides basic tools that will help the CSM manage change. 


1. **[Customer Success Basics](https://www.linkedin.com/learning/customer-success-management-fundamentals/customer-success-management-fundamentals):** What is customer succcess and why does it matter? This 1-hour training introduces you to key customer success concepts. Through this 1-hour training you will understand the 14 tenets of customer sucess and how they impact your role. 

1. **[Generating Business Value](https://www.linkedin.com/learning/business-fundamentals-for-customer-success-managers/overview-of-how-businesses-generate-value):** Having a customer-centric business outlook is critical in today’s business landscape. It is critical for customer success managers to have understand how businesses work and how value is created and measured. Through this 1-hour course the CSM can learn about the connections between customer success and business awareness, the reasons why businesses exist and how businesses create value.

1. **[What is a Customer Success Manager?](https://hbr.org/2019/11/what-is-a-customer-success-manager):** This HBR article outlines the CSM role. 



## Know your Tools

1. **[Gitlab](https://www.markdownguide.org/cheat-sheet/):** This Markdown cheat sheet provides a quick overview of all the Markdown syntax elements

1. **[Slack](https://slack.com/intl/en-ke/help/articles/360059976673-Slack-video-tutorials):** These short 2-minute videos help you get started with Slack. 

1. **[Zoom Basics](https://livetraining.zoom.us/rec/play/7gMetRlmoTpTCweZneY_-GOhVJStq-n22v2iWjLPzPoZzcrGxnI2ki1D5R4SRs2iL4oRD0d6hmcYIDtl.30KvMEGlF__2vM1Y?continueMode=true):** Zoom is one of the most commonly used collaboration tools at DoubleGDP. This 50-minute session provides the basics that will get you started for your zoom meetings. 


## Remote Working Fundamentals

1. **[Well-Being](https://www.linkedin.com/learning/thriving-work-leveraging-the-connection-between-well-being-and-productivity/the-story-of-thrive-global?contextUrn=urn%3Ali%3AlyndaLearningPath%3A5e67c339498ee624c52b641c):** The modern work environment poses many challenges to the customer success manager. Through this 41-minute training you will have an appreciation of how to leverage the connection betweeen your well-being and productivity. 

1. **[Working Remotely](https://www.linkedin.com/learning-login/share?forceAccount=false&redirect=https%3A%2F%2Fwww.linkedin.com%2Flearning%2Fremote-work-foundations%3Ftrk%3Dshare_ent_url%26shareId%3Dz48OncF8SAqlIJ%252BWgPrN4A%253D%253D ):** DoubleGDP is an all remote company, this working environment has its unique qualities. Explore this training whether you have been working remotely for several years or if this is your first remote working role. 

