# Finance


## Invoice Submissions and Invoice Approvals

**Invoice Submissions**

*   All invoices are required to submit to [accounting@doublegdp.com.](file:////doublegdp/handbook/-/blob/master/accounting@doublegdp.com) 
*   Please DO NOT submit invoices and/or payment instructions via GitLab tickets
*   Vendors are required to submit their invoices within 7 days after the services are rendered or the goods are delivered. Timely submission of your invoices will enable Accounting to record the expenses in the right financial period.

*   **Important Tax Form Requirements**

1. For **US-based vendors**, vendors are required to provide a completed [W-9 Form](https://www.irs.gov/pub/irs-pdf/fw9.pdf) to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) prior to or at the time when they submit their 1st invoice.
1. **For Non-US vendors**, vendors are required to provide a completed [W-8BEN-E Form](https://www.irs.gov/pub/irs-pdf/fw8bene.pdf) prior to or at the time they submit their 1st invoice to [accounting@doublegdp.com ](mailto:accounting@doublegdp.com). Here are the [instructions](https://www.irs.gov/instructions/iw8bene) for Form W-8BEN-E Form.
1. **Important: Accounting cannot process a payment without these Tax Forms**

**Invoice Approvals**

* All invoices that require Accounting to make a payment are required to be processed in Bill.com
* Approvals of invoices are completed in Bill.com. 
* Accounting will route the invoices according to the following approval hierarchy
* In general, invoices up to $5000 can either be approved by [Executive Assistant](https://handbook.doublegdp.com/people-group/roles-and-responsibilities/) or [CEO](https://handbook.doublegdp.com/company/teams/#ceo)
* Invoices over $5000 require [CEO](https://handbook.doublegdp.com/company/teams/#ceo) approval
* **Project and Program Related Invoice Approval**. For Project and Program-related invoices, ONLY the Head of CS team approves the bill in Bill.com. The EA and the CEO can approve the payment in SVB for wires. 
* The Head of CS and Accounting can review and validate the spending according to the Project and Program budget periodically. However, all Project and Program-related bills will need the Head of CS to approve in Bill.com to enable proper internal control.

**Invoice Receiving Acknowledgement and Documentations (Accounting’s Responsibilities)**

*   Process invoices received in [accounting@doublegdp.com.](file:////doublegdp/handbook/-/blob/master/accounting@doublegdp.com) in Bill.com and route the invoices for approval in Bill.com according to the Invoice Approval Hierarchy.
*   The AP accountant will reply to the [accounting@doublegdp.com.](file:////doublegdp/handbook/-/blob/master/accounting@doublegdp.com)and the vendor to acknowledge that the invoice has been processed in Bill.com. The turnaround time is usually 1-2 business days from the day the invoice is submitted to Accounting and the acknowledgment.
*   AP Accountant needs to make sure to use the contractor's contact email to send out bill.com e-invite (do not use the contractor's DoubleGDP email). This will help us to make sure the applicable contractors can receive their tax forms even if they are no longer working with DoubleGDP
*   For US vendors, upload a copy of W-9 in Bill.com under the Document section
*   For non-US vendors, upload a copy of   [W-8BEN Form](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf) in Bill.com under the Document section

## Invoice Payments

### Bill.com Payments

*   Once an invoice is approved, payment is usually processed based on Net 30 payment terms unless otherwise specified in an agreement or invoice
*   Bill.com e-payment is recommended to pay all vendors
*   How Bill.com e-payment works? 
    *   To establish Bill.com e-payment, Accounting will send an e-invite to an email that is provided by the vendor. The vendor accepts the invite and provides their bank information within Bill.com. The payment will be scheduled to deposit to the vendor's account according to the payment terms. E-payment will usually arrive in the recipient's account within 2-4 business days.

**Exception:**  For some non-US vendors who may have difficulties setting up a Bill.com e-payment, a wire payment will be processed. 

### Wire Payments

All wires from SVB are initiated by Accounting and approved by DGDP authorized SVB signer(s) per the authorization that is set up with SVB.

#### Wire requestor's responsibilities:

1. Make sure the vendor provides all the required information as outlined below before requesting a wire payment
2. For a new vendor's 1st payment, please send an email to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com) with invoices, wire instructions, Form W-8Ben-E (if non-US vendors). For the 2nd payment and onward, send invoice only is okay unless the vendor requests any changes, such as bank changes
3. Make sure the vendor provides a formal invoice. (A quotation and/or a Pro Forma Invoice cannot be accepted to issue a payment) Exceptions should be discussed with the EA and Accounting.
4. For non-US vendors, make sure the invoices indicate the proper currency 
Wire payment is used to pay any vendors who cannot get paid via Bill.com or to provides funds to **non-US team members** for business expenses. To request a wire payment, please send an email request to [accounting@doublegdp.com.](file:////doublegdp/handbook/-/blob/master/accounting@doublegdp.com) along with

1. The invoice to be paid
2. Vendor's wire instructions - see below-requested information about wire instructions
3. If a foreign currency wire payment is required, please indicate which currency
4. If a wire payment needs to arrive by a certain date, please be very specific about it so Accounting can plan accordingly
5. A brief description for the Purpose of Payment (per bank requirement for international wires)
6. **Bank Fees** If any bank fees that DGDP should be covering to ensure the recipient can receive the full payment, please notify Accounting about how much bank fees to add to the wire total as the fees can vary vendor by vendor. For example, some Zambia vendors don't need DGDP to include any bank transfer charges, some of them do. Given each vendor's situation is different, Accounting requests vendors to provide how much bank fee to include and in what currency with the 1st payment request. For future recurring payments, Accounting can include the same amount of bank fees without asking again unless new changes are provided by the vendor and the amount of change is justified by Accounting.

    Typically, a wire instruction needs to include below:

1. Recipient's name
2. Recipient's address
3. Recipient's bank name
4. Recipient's bank address
5. Recipient's bank account
6. Recipient's bank ABA Routing Numbers (if domestic wire within the United States)
7. Recipient's IBAN, BIC, or SWIFT Code (if international bank outside of United States)
*   Sometimes the bank may require to provide additional information, Accounting will reach out to the wire requestor if such request arises. *
*   Wire payment is usually quicker, but for international wire payment, it still takes a few business days to clear. We suggest you submit the wire request at least 7 business days prior to the payment due date to allow accounting to get the wire initiated and approved.
*   If the wire payment is urgent, please flag "**URGENT Payment Request**" in the subject line when you send the wire request to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com). Please don't submit a payment request via a Gitlab ticket as it can easily be buried in the thread.
*   If the wire payment is recurring, please send the invoice that needs to be paid to Accounting ([accounting@doublegdp.com](mailto:accounting@doublegdp.com)) and indicates this is a recurring payment with no wire instruction change. Please be specific if the wire needs to arrive by a certain date so Accounting can plan the wire accordingly.

Once a wire payment is initiated and approved, the controller will send a proof of payment to the wire requestor in the following business day or when such document  becomes  available in SVB. When the controller is not available, such as on vacation, the EA will provide such proof of payment.

## Business Expense Reports Paid with Personal Cash or Credit card

*   For expense reports, receipts should be submitted through **Expensify** 
*   If you paid business expenses with your personal card, submit your receipts through Expensify. And submit your Expensify Report for approval within the system.
*   You can do this through the app, or by forwarding the receipt to [receipts@expensify.com](mailto:receipts@expensify.com) from your DGDP email address.
*   The reimbursement for **US-based team members** will be processed in Bill.com unless otherwise advised.
*   The reimbursement for **non-US team members** will be processed in Pilot unless otherwise advised.
*   Please submit all receipts by the 7th of the month as Accounting has the deadline to close the books monthly. For example, the expense report and the receipts for October 2020 need to be submitted by November 7th, 2020

## Expense Report Approval & Reimbursement

*   Expensify approval process is automated
*   Once you submit your expense report, the system will route your expense report to your direct manager for approval
*   After the manager approves in Expensify, Accounting will validate the receipts for accuracy and process payment within 7 days through one of the following methods: 
    *   Bill.com for **US-based team members** 
    *   Pilot for **non-US team members** 
    *   Frequency of Pilot Reimbursement  
     Given the company has implemented the monthly stipend payment to cover the bank fees associated with the monthly Pilot payroll, effective September 1, 2021, Accounting will process monthly reimbursement along with the monthly Pilot payroll to effectively manage the bank fees the company needs to pay. 
    ** Exception**: If the reimbursement amount is significant to the team member who needs to get the payment sooner than the monthly payment schedule, please send an e-mail request to Accounting@doublegdp.com for an off-cycle reimbursement payment.
    *   Your hiring agency, for non-US team members who are working with DGDP via an agency
    *   For non contracted DoubleGDP team members, Transferwise will be used to process reimbursement unless otherwise agreed

## Company Owned Credit Card, Debit Card, and Wise Account Usage Guidance

**Company cards should be mostly used for these categories**:

*   Travel (airfare, lodging, dining, ground transportation, and other misc. travel expenses)
*   Business subscriptions, online ads, etc.
*   Business meals, food delivery, and venue, for employee events, business events
*   Office and business supplies
*   Other online payments, such as business tax payments, business licenses, etc.

**Exceptions:**

**_DGDP’s Project and Program Expenses:_**

*   This exception is mostly applied to the CS team who manages the expenses for DGDP’s projects and programs. For project and programs related expenses, a business credit card or debit card can be used for expenses &lt;$500 per unit price. For example, small furniture with a unit price of less than $500 USD.
*   We encourage local teams to use vendors who can accept credit card payments. \
For example, for Zambia vendors, the payment approach should be followed as below:

**Expenses &lt;$500 per unit price**:

*   1st option: [Ramp Card](https://ramp.com/) 
*   2nd option: [BOA Debit Card](https://www.bankofamerica.com/smallbusiness/) 
*   3rd option: [Wise](https://wise.com/) 
*   If neither Ramp nor BOA card works for a certain vendor, the purchaser should reach out to the CEO’s EA to process a [Wise](https://wise.com/) payment and copy Accounting. Once a Wise payment is made, the purchaser should send a receipt to the EA and Accounting.

**Expenses >$500 per unit price**:

*   Vendors should submit an invoice to Accounting and go through the company’s [Invoice process](https://handbook.doublegdp.com/finance/#invoices) outlined in the Handbook.
*   If a special situation arises that will require an urgent credit card payment for purchases over $500 per unit price, please contact CEO’s EA and Controller to get approval before the credit card payment.

### Business Expenses paid by DGDP's Ramp Visa Card

*   [Ramp Visa credit cards](file:////doublegdp/handbook/-/blob/master/www.ramp.com) are issued to **Customer Success Team**, only to pay purchases since Visa card is widely accepted in Zambia and Honduras.
*   **Always** use RAMP VISA Credit Card as your **first** option for payment. We strongly encourage finding vendors that accept Credit Cards payments.
*   ALL business expenses require a receipt submitted to [receipts@ramp.com](mailto:receipts@ramp.com) 
*   All transactions require a memo
*   Make sure to submit an itemized receipt, not the credit card payment
*   For business meals or client meetings, please list the business purpose and the names of all the participant in the receipt
*   It is recommended that the most senior level colleague must pay for the expense

### Business Expenses paid by DGDP's Prepaid Card / BOA Card (_Applies to Zambia's CSM_)

*   If you paid business expenses with the company's pre-paid card, please submit your receipts through Expensify.
*   Very importantly, please name the report with the wording such as " Receipts Submission Only". For example, "October 2020 Expenses_ Receipts Submission Only". Including this specific wording will be very helpful for Accounting to differentiate the expense reports that do not require reimbursements versus the ones that they do.

### Business Expenses paid by DGDP's Brex Card

*   If you paid business expenses with the company's Brex card, please forward the receipts directly to [receipts@brex.com](mailto:receipts@brex.com) or upload your receipts directly to [Brex](https://www.brex.com/), also make sure you add a memo with a brief description of the expense.
*   ALL business expenses require a receipt submitted to Brex
*   All transactions require a memo
*   Make sure to submit an itemized receipt, not the credit card payment
*   For business meals or client meetings, please list the business purpose and the names of all the participant in the receipt
*   It is recommended that the most senior level colleague must pay for the expense

### Business Meals and Entertainment Guidelines


1. It is important to have team-building events. For those of us who have others in our area getting together to visit or celebrate a milestone can form important social connections. DGDP supports the costs of these events periodically.
2. The most important thing when spending is that you act reasonably with the company's money. It's hard to give strict guidance on specific prices because what's reasonable will vary from country to country and city to city. You're responsible to interpret those prices relative to your location. That said, here are some guidelines for your consideration:
*   Choose moderately priced places for general team meetings or visits. For bigger occasions, pick a place that is appropriate for the cost.
*   For restaurants, this probably means $$ or $$$ on [TripAdvisor](https://www.tripadvisor.com/ShowTopic-g1-i12105-k12250210-Dollar_Sign-Tripadvisor_Support.html) but not $$$$.
*   It's okay to spend on drinks, but remember that you are in a professional context and act accordingly. There can be a huge range in price on wine and liquor; stay at or below the mid-range of cost.

## Contracted Non-US Team Members


*   For contracted **non-US team members**, a completed W-8BEN Form is collected during the [Pilot](https://handbook.doublegdp.com/finance/#pilot) onboarding process.
*   Payments related to the contracted services are usually processed in [Pilot](https://handbook.doublegdp.com/finance/#pilot) monthly.
*   Expense reimbursement reports from the **non-US team members** are required to submit in Expensify.

Please refer to the Expense Reports and Pilot sections below for more details.

## Pilot

DGDP uses [Pilot](https://pilot.co) to process payments for **non-US team members.**

When a team member successfully completes the onboarding process, the direct manager of the new hire should notify Accounting immediately via sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) with the information below:


*   Full name of the new hire
*   Official start date
*   Annual or monthly compensation in USD$

With the new hire's information, Accounting will help to pre-generate the monthly invoices for the next 12 months on behalf of the new hire. If there are any exceptions on this, please notify Accounting by sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com).

Since the team members in Pilot are mostly based outside of the US and the processing time of non-US payment varies, Accounting has developed a monthly payment calendar to streamline the payment schedule and to allow the funds to arrive on or before the last day of each month.

For new hires who joined on or before the 22nd of a given month, Accounting will try to include the new hire in the monthly payment schedule. If a new hire joined after the 22nd of a given month, Accounting may need to run an off-cycle payment for the new hire. Accounting will notify the new hire when the payment will arrive in the situation of an off-cycle payment.

If you have any questions or concerns about the status of your payment, please contact [accounting@doublegdp.com](mailto:accounting@doublegdp.com).

## Payroll


*   Payroll for US-based full-time employees is managed via Gusto.
*   DoubleGDP runs a semi-monthly payroll schedule.
*   Employees will complete the onboarding process through Gusto and set up direct deposits for their payroll payments.

## Fixed Assets Tracking & Management


*   Fixed assets such as office furniture, computer equipment, laptops, are purchased for long-term use. They usually have a useful life of more than one year. DoubleGDP tracks its fixed assets with a unit price over $500 USD via this document [Employee Equipment - Fixed Asset Tracking](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?ts=5fab0d81#gid=0).
*   The purchaser of the assets is required to log in to this file to fill out the information (as indicated in the file), such as date of purchase, purchaser's name, the value of the asset, etc.
*   Any team members who received the company purchased laptops or computer equipment are required to log in to this file and fill out the asset description, model #, and serial #.
*   Please make sure the information of the Fixed Asset Tracking file is completed within 7 business days of the purchase date when the asset is initially purchased.
*   If a transition happens from existing computer equipment to another team member, the recipient of the computer equipment is required to acknowledge the receipt of the computer equipment within 7 business days. Please log in to the Fixed Asset Tracking file and fill out the required information (as indicated in the file), such as the description of the asset transition, date of transition, old owner, new owner, etc.
*   **Purchasing Company Assets that are no longer used for Business **

    If an employee is interested in buying a company asset that is no longer used for business, the CEO will review case by case, with consideration to whether an asset can be used again, value to the company, and define a fair market value for sell. CEO should notify Accounting immediately if a decision is made to allow an employee to purchase the company's assets, so Accounting can make sure this is recorded properly.


## Business Documentations

Important business documents, such as business registration, tax documents, business insurance, business letters, and various agreements, etc. are required to be documented in the DGDP Google Drive. \
DGDP team members are required to forward important documents to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com) for documentation. The accountant in the Accounting team is responsible for the documentation and is expected to complete the documentation within 2 business days from the receiving date. How to use the Main Folders under DoubleGDP LLC G-drive



*   **Taxes:** all tax-related documents should be saved here. Examples: Income Tax return, 1099 Filings, CA State Tax Filings, R&D tax credits, etc.
*   **People Operations:** all employment-related documents. Examples: Worker's Compensation, Labor Compliance, Employment Agreement, Contractor Agreements, etc.
*   **Insurance:** all business insurance documents. Examples: Hub, Guardian, etc.
*   **Business Letters:** important business letters. Examples: Enyimba Economic City Letter Nolan forwarded to Accounting
*   **Business Formation/Registration/Renewal:** business registration documents. Examples: EIN letter, CA EDD documents, Registration Renewals
*   **Business Agreement:** business agreements. Examples: Ciudad, Customer Testing agreement
*   **Accounting:** The files that Accounting actively manages. Examples: Accounting month-end close files, Bank Files, Bank statements, and all other confidential documents
*   **Vendor Agreements:** important vendors and vendor agreements. Example: agreement associated with Ufudu, Thebe, etc.

    Try to maintain documents based on these main folders. Sub-folders can be created as needed. If a new main folder needs to be created, please contact Controller as well as the CEO's Executive Assistant for creation.


**Important Dates:**

Accounting is responsible to coordinate and manage these important dates and make sure they completed on time.



1. CA LLC Secretary of State renewal - Due every two years by the formation anniversary date (DGDP is formed in CA July 17, 2019)
2. Annual 1099-NEC Filing - typically due by Jan 31 each calendar year with IRS
3. Annual LLC income tax return - typically due on April 15 each year
4. San Francisco Annual Business Tax Return - typically due on April 15 each year
5. DGDP quarterly capital contribution - 1st Monday of 2nd month of each quarter. Google Calendar reminder is set up for this between Controller and CEO's Executive Assistant. The controller will remind the CEO during monthly finance reporting too.


## Company Mailing Address

We have a mailing address through [Earth Class Mail](https://www.earthclassmail.com/). This service scans our mail and sends it to us as an email attachment. Here is our address:

Please use our dedicated mailbox for all accounts requiring a **mailing address** or **billing address**:

    9450 SW Gemini Dr
    PMB 28766
    Beaverton, Oregon 97008-7105 USA

Note: the company does not have any method of receiving packages or physical items.
