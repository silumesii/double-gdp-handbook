---
title: Goals and OKRs
---

# Company Goal

Our north star goal is to increase the population of cities that leverage the DoubleGDP platform. Our goal in 2021 is to end the year with a population of 100 people. Our goal, along with subgoals and strategies, is captured in GitLab through this epic: [&68](https://gitlab.com/groups/doublegdp/-/epics/68)


## 2021-Q2 Company OKRs

All OKRs are listed are captured in GitLab as children epics of our [Company Goal](https://gitlab.com/groups/doublegdp/-/epics/68). A video introduction to our process and walk-through of Q2 OKRs is available [on Loom](https://www.loom.com/share/7248063131bb477898825d11406028f3).

## Performance Management Framework

In order to achieve this ambitious goal, we must think about how we break it down, and manage performance to it. At DoubleGDP, managing performance has three primary components:

- Team-level Objectives and Key Results (OKRs) that we use to drive company growth and development of new capabilities
- Team-level Key Performance Indicators (KPIs) that we use to measure health of existing organizational capabilities
- Individual performance feedback, that we use to grow as individuals and develop our careers

Our process is modeled after GitLab's but simplified because we're so much smaller. You can learn more about their process through their pages on [OKRs](https://about.gitlab.com/company/okrs/), [KPIs](https://about.gitlab.com/handbook/ceo/kpis/), and [360 feedback](https://about.gitlab.com/handbook/people-group/360-feedback/).


### Key aspects of DoubleGDP performance management

The most important aspects of our performance framework are:

- We have one north star -- population growth. This helps us measure the results of all of our other efforts and prioritize what matters. At the end of the day, our contribution to the world will scale with the number of people living in cities powered by DoubleGDP.
- OKRs are written and evaluated each quarter and are intended primarily for organization functions or teams rather than individuals. They articulate measurable goals we aim to achieve in order to materially advance our capabilities. They should be focused and ambitious -- i.e. we should expect between 1 and 3 OKRs per function and set them to stretch us beyond what we are sure we can achieve.
- KPIs are also at a function or team level and measure the health of existing capabilities. Each function has responsibilities that they must contribute in order for the company to run. Finance pays invoices and closes the books; engineering rapidly builds high-quality features; our product serves users. Each function will keep a variety of Performance Indicators (PIs) to evaluate whether it's delivering what's needed. Those that are most important will be considered "Key" (a "KPI") and raised to the awareness of other departments.
- Individual performance feedback focuses on creating a productive dialog between a team member and their manager. It aims to build self-awareness and foster professional development. We do not have numerical scores or a formal process that ties personal goals with team OKRs or PIs. Instead, the process will collect input from the team member and others familiar with their work around strengths, areas for improvement, and general. We also facilitate feedback from team members to their managers.

Note that we do not have a compensation review process at this time.

### Why we don't link OKRs with personal goals

Our performance management framework aims to emphasize that results are achieved through teamwork. Managers and team members may choose in certain cases to articulate personal goals, but we don't have a company-wide policy to set goals at an individual level or to give numeric scores to attainment of them. Individuals are accountable to helping the team achieve its goals, and for approaching their work in ways consistent with our values. We think these outcomes will be best achieved through promoting a healthy dialog between a team member and their manager, informed by the input received from peers and with consideration to overall team performance.

Some more of the rationale behind this approach:

- We want to prioritize team results over individual outcomes
- We don't want to create disincentives for taking on ambitious goals, or for playing a supporting role
- Effective teams often realign their individual work based on the challenges they're facing, and individual goals set quarterly can make it difficult for people to reprioritize quickly
- It takes discretion and context to properly understand someone's contribution, and putting emphasis on evaluating individual goals can distract from most important things
- For clarity, managers do have accountability for achievement of team results


## Performance management process

### How we implement OKRs and KPIs
Notes on our implementation of OKRs and KPIs:

- An objective and key result should be able to be read in this phrase: "We aim to achieve <u>&nbsp;&nbsp;&nbsp;*objective*&nbsp;&nbsp;&nbsp;</u>, as measured by <u>&nbsp;&nbsp;&nbsp;*key result*&nbsp;&nbsp;&nbsp;</u>"
- Each `Objective` or `KR` will be prefixed with its year and quarter in the format yyyy-qq
- Each `Objective` is captured an an Epic that articulates what we aim to achieve. If the objective has a top-line result against which it will be measured, we will add `OKR` to its prefix and include that KR in its summary. Some objectives may have several key results and not one primary; in this case we add `Objective` to its prefix.
- A `Key Result` or `KR` indicates how we evaluate progress of an objective. Each is represented as an issue and should have a clear date and way to measure or observe that it's been met. Each KR issue is nested under its parent Objective epic.
- KPIs (and PIs) are defined and listed on a function's handbook page. KPIs are shared in a function's sprint update (either from handbook or on slide deck)
- The top-level OKR epic and its children are used to promote visibility on what we aim to achieve, and we use the `Health status` of its issues to monitor progress to goal. In order to keep the OKRs easy to understand, we do not link the operational issues and epics that track our work into this hierarchy. (i.e. the work to accomplish the KR should be tracked separately.)


### Process when we miss goal
Our north star metric has monthly milestones. We aim to hit these month over month. However, growth is not always continuous and there are times when we won't hit the goal. When this happens, it's a good opportunity for us to understand what went wrong (if applicable) and refocus our energy and creative spirit to get back on track. Our process when this happens is as follows:

1. Each teammate takes 10-15 minutes to [brainstorm 5-10 ideas](https://forms.gle/4Tpd8PedNHYZXxtH8) to improve.
1. Flag ideas that you particularly like with `**`
1. CEO will review all ideas and choose a few to focus on
1. In the next Sprint Retrospective, we will brainstorm what would be some iterative approaches to those ideas, focusing on simple and actionable near-term steps. For example: can we get 50% of the value of the idea at 10% of the cost? What's the smallest iteration that can make some progress toward the goal?
1. We'll write a few stories to add to the sprint about to kick off
