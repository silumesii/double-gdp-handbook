---
title: KPIs
---

## EA KPIs

1. Handbook contributions per team member per sprint (target: 1)
1. Percent of Slack messages in public channels (no target)

## Customer Success KPIs

This pulls together CS KPIs and Program KPIs.

1. One merge requests per customer success manager per sprint in either [city processes](/city-processes/) or [customer success](/customer-success/) pages (target: x)
1. HIR applicants by stage (no target)
1. AIR sculptures & murals in place (target: x)
1. AIR-influenced clients in construction pipeline (target: x)

## Product KPIs

1. Number of payments and invoices recorded (no target)
1. Number of gate accesses and ratio that are from QR-codes (no target)
1. Weekly active users and daily active administrators (no target)
1. Weekly news articles read (no target)
1. Messages exchanged (no target)
1. Feedback received (no target)
1. Time cards submitted (no target)

Product KPIs are regularly tracked in a [product operations dashboard](https://docs.google.com/spreadsheets/d/1tR8KoJ8JWTFGmlAETWxfNx9UiC9JrJkHAiOiikXnIvQ/edit#gid=627688641). 

## Engineering KPIs

See [Engineering KPIs](/prod-eng/engineeringkpis/)


## Finance KPIs

1. Invoices paid
1. Monthly closes complete

## Partnerships KPIs

1. Meetings Booked
1. Leads Qualified 
1. Opportunities Created 
1. Active Partners
