---
title: CEO Readme
---

## Intro

The purpose of this handbook page is to document CEO processes and background that is helpful to teammates or external partners working directly with the CEO.

## CEO Staff Meeting
The CEO hosts a weekly staff meeting of 75 minutes with all of his direct reports. This meeting has two primary objectives:

1. [Tactical] Help all functions of the company to stay coordinated around our efforts to hit our goals. Share progress, challenges, ask questions, explore creative approaches that may come from cross-disciplinary perspectives.
1. [Strategic] Discuss themes or issues around the broader context in which we operate and the impact we aim to have on the world. This is both to stay abreast of issues that impact our strategy, and to build the teamwork communication practices that will help us make effective strategic decisions quickly when they’re needed.

**Timing**. In order to use our time effectively and help build effective asynchronous practices, we ask some preparation in advance of the meeting:

- Monday at 2 pm Local Time - each person updates the progress using "Health Status" in Gitlab issues. The Health Status should be a clear reflection of where things are (On Track, Needs Attention, or At Risk) and share a brief written update with highlights of the last week, concerns, or tactical topics for discussion with others. Items added to agenda should be marked with an estimated time request if it's more than an FYI (assumed to be 2 mins or less)
- Mondays by 5 pm PT - CEO reviews Health Status, agenda, and prioritizes if requested topics exceed the time available
- Tuesdays at 7 am PT - team meets

**Default Agenda**. Roughly speaking, we spend about 45 minutes each week on tactics and 30 on strategic. This is not strict, but a template that provides a default expectation for the meeting. This breaks down as follows:

Tactical [45 mins] -- Agenda to be driven by staff updates or questions from the tactical updates the day prior to the meeting

- [10 mins] Team connection. ~2-minutes each to share and connect on personal updates
- [10 mins] Construction pipeline. Head of CS to share progress on core goal (&56), challenges, and opportunities for help.
- [25 mins] QA on pre-meeting asynchronous updates, FYIs (ad hoc)

Strategic [30 mins] -- Agenda to be driven by CEO, with input or requests from team the week prior to the meeting

- 1-2 strategic topics teed up by CEO (preferably with pre-thought done by team. I.e. goal is to share topics a week in advance, with some reading material and even discussion done in advance. Hopefully this will be with nominations from team -- i.e. topics that people are wanting to discuss in more depth.)

**Getting on agenda**
- To request time during the meeting, add it to the agenda document the day before. Requests of up to 10 minutes are auto-approved (i.e. just add them); requests over 10 minutes should be discussed with CEO in advance.
- Label your agenda item with time requested (e.g. "[10]" minutes) and a label for what type of discussion you'd like to have. For example, `FYI`, `EXPLAIN`, `HELP`, `RESOLVE` 
- During the meeting, the person who added the agenda item has the floor; they are responsible for returning the floor back to CEO at the end of allotted time.
- EA plays "time cop" during meeting, giving a warning when time on a topic has expired. We allow two minutes grace-period and then move on.

## Radar
A few notes on how the CEO keeps track of his radar:

- The [COMPANY GOAL epic](https://gitlab.com/groups/doublegdp/-/epics/68) collects the most important goals and initiatives for the company.
- The ["CEO Desk" Epic](https://gitlab.com/groups/doublegdp/-/epics/83) tracks projects that are being driven by CEO and EA.
- The `ceo-interest` label is applied to other issues and epics in GitLab that are being driven by someone else but have particular interest to the CEO.

## Routine Processes

### Sprint updates
At the end of each sprint, CEO will publish and share an update according using the ["CEO Sprint Progress Update" deck](https://docs.google.com/presentation/d/1wrCcckL8qgNKSjWaHtbj0KS-F0A77YCuxJf5-0mwVdI/edit). It will be consistent with the process outlined in the [Sprint Demo](/prod-eng/productdev/#sprint-demo) and promoted according to our [social media guidelines](/marketing/social-media/#sprint-updates)


### Virtual coffee talks
To keep a personal connection with how people are doing, the CEO holds virtual coffee talks with each person in the company. The aim is to do these at least once per quarter, and more often as time permits. These do not have a formal agenda, but you are encouraged to share your thoughts about what's working well and what could be improved culturally. We'll use the 1:1 Agenda document to collect thoughts if there are specific topics. Preparation is encouraged but not required.

## Reference Material

### CEO Biography

Sometimes it's necessary to share a CEO biography for a sales or partner getting to know us. The latest one is available in our [Shared Drive](https://docs.google.com/document/d/14238YHRkOUYO5TI_tgAi2WKeAIV_oSFUM5IVNAjMHNw/edit).

## Meeting requests

If you would like to schedule a call with me please include the following information in your request:

* Subject of the meeting
* Must have/optional attendees
* Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Provide context:
        
1. Every meeting has Google Doc accessible to the direct report and myself only
1. The doc is filled by the report 1 working day before the meeting
1. Clear expected outcome of the meeting
