---
title: Product Vision
---

## Product Vision

We are building an end-to-end platform for new cities to connect with residents, accelerate growth, and deliver responsive public services. Central to this is a platform that acts as a hub -- for the community, for municipal managers, and for a technology ecosystem.

This diagram provides a high-level overview of how we see those aspects coming together, with a description about how core technology, DoubleGDP modules, third party software providers, and custom-built apps will come together.

![Platform as a Hub](/img/company/platform-as-a-hub.png)
