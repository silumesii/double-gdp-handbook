---
title: Artist In Residence Program
---

## Artists in Residence Program at Nkwashi
The purpose of the Artists In Residence (AIR) Program is to showcase life at Nkwashi as an emerging vibrant cultural scene that garners interest and action for people to move to Nkwashi.


### Higher Level Goals
* Chena to show life at Nkwashi. Build cultural cachet, sense of permanence, activity on site, and generate interest in Nkwashi. Signs should be tangible and permanent, visible on social media.
* DoubleGDP to work with Nkwashi Marketing to convert the interest generated from the program into people moving to Nkwashi. This means progressing through a funnel of purchase plot, start construction, finish construction, move in.
* DoubleGDP can use the AIR program as a "City Building Laboratory” to try out new product features or strategies.
* Chena runs the day to day of the program without a lot of time required from DoubleGDP. Our time should be focused on the conversion of interest in the program into leads.

### Outputs
* Artwork and FOMO Progress can be tracked here: https://docs.google.com/spreadsheets/d/1Um4WxIBY6ZIxJcDjADvKKD8h9Me_9VVW/edit#gid=652938971 
* Chena is to update the Progress tabs on a weekly basis, before every sync meeting on Tuesdays. 


### Roles and Responsibilities
* Creative Director - Sepo (CHENA)
    - Selection of artists
    - Planning aesthetics - what artists & artisans
    - Overseeing the program
    - Create flyers
    - Contact hosts for events
    - Assist with budget preparation
    - Facilitating events / curation
    - Contract creation
    - Planning/goal: Art map of Nkwashi - 3,100 acres plan of art and phases. Town curation and vision. Being thoughtful of creations to create a bigger vision.
* Planning & Strategy Director - Zelipa (Chena)
    - Budget preparation
    - Plan the events
    - Work on the calendar of events & workshops
    - Select the artists
    - Legal contact
    - Planning/goal: Meet with other curators
* Operations Coordinator - Jemimah (Chena)
    - Purchase items needed for events
    - Looks after artists - gets staples
    - First contact for artists - liaison
    - Office admin - paperwork
    - Make sure artists sign before the deadline
    - Daily communication with artists
* AIR Program Manager - Zoussi (DGDP)
    - Content creation and management (WordPress)
    - Marketing coordination with Nkwashi Marketing team
    - Liaison between DGDP leadership and Chena
    - Track engagement on social media
    - Proofread posts
    - Email marketing
* Social media lead - Mwabi (Chena)
    - Promoted ads
    - Content creation
    - Schedule posts on Instagram and Facebook
    - Collect visual content from artists
    - Video editing
* Nkwashi Marketing & Sales Liaison - Thelma (Nkwashi City/Thebe Investments)
    - Approve content
    - Give general marketing direction
* Content creators - artists (Esnala and Julius)
    - Write WordPress articles (Esnala)
    - Record video content and video editing (Julius, in-house videographer)
Note: Videographer / video editor is to be replaced by Chena should the in-house videographer not be able to produce the content needed.

### AIR III Value Measurement
#### Overview
The goal of the AIR program is to inspire individuals to move into Nkwashi and to drive residency through inspiring artistic and marketing content. In principle the artistic output will be delivered on a regular basis as determined by the kind of art (sculpture/mural/digital art) and start date. Its outputs will be:

- 7 iconic pieces -- large inspirational works
- 5 large sculptures
- 18 murals
- 10 smaller works around the park
- 4 artistic directional signs
- 28 influenced residents

Responsibilities among parties:

- Chena will:
    - Ensure the artists have all the requirements to deliver the art pieces  
    - Be responsible for the timely delivery of art pieces as listed in the budget document - on the sheet “Stipends and Outputs”
    - Report regularly on progress and highlight any challenges that may necessitate change or delay

- DoubleGDP will:
    - Ensure timely delivery of the AIR III program’s funds.

#### Artistic and marketing output
* Community Projects (All artists in residence will contribute)
    - Eagle Man (Soar High)  8 meters or above
    - 2 Public toilet blocks
    - Nkwashi Wall Mural on Leopards Hill Road Turn off
    - Tau District Entrance Mural
    - Eagle Suburb Entrance Mural
    - Prayer Man
* Group Projects (Pairs and teams will be given the following targets)
    - Life-sized pieces (Murals/Statues/Sculptures)
    - 4 Artistic directional signs on Silicon Way/Main Access Road
    - Sculpture on Ndele Road
    - Sculpture on Nile Road
    - Sculpture on Rokel Road
    - Mural on Bay Area on Rokel
    - Sculpture on Cavalla Road
    - Mural on Zambezi Road
    - Sculpture on Zambezi Road
    - Mural on Orange Road
    - Mural on the corner of Kisangani and Cavalla
    - Mural on the corner of Okavango and Nile
    - Sculpture on the corner of Rokel and Okavango
    - Sculpture on the corner of Ndele and Okavango
    - Mural on Silicon Way (Exit from Eagle)
    - Small sculptures on the water front
* Marketing and social media content
    - 30 second well edited short video once a week to highlight TJ as she works at the Chena Art House at the Nkwashi waterfront and shares her life and growth in her career at Chena
    - 2 minute well edited short video once a month to capture natural/wild life and play based learning at little explorers (from parents who have given consent for learning and play activities)
    - 30 second well edited short video once a week to show community projects, individual progress and other art come to life
    - 5 minute well edited mini reveals once a quarter with invited visits for "spot the art" drive arounds with drone coverage and a commentator
    - 2 short blogs tailored to residents with guidance from Zoussi and Sepo based on feedback from the newsletter to touc the hearts of family and friends as subtle marketing of the creation of an exclusive lifesyle opportunity given for those who live at Nkwashi
    - 2 iconic photos  a week well edited for display on all Nkwashi platforms and physically in slideshow mode and where funds allow, printed and framed on the sales floor
    - 1 workshop a week by an Artist in Residence (to be live streamed)

#### AIR III impact on residency
In order to measure the impact of AIR III on residency at Nkwashi, DoubleGDP will:

* Assess the number of prospective clients and residents learned about the Nkwashi from the residency program in the Nkwashi app user creation and in Thebe sales questionnaires.
    - This number will be tracked on a monthly basis.
    - Target to increase the number by 230% between March and December 2021 (AIR III should inspire 28 individuals to move into Nkwashi)
* Track online content by measuring the feedback; click rate and conversion rates from AIR posts in the Nkwashi newsletter.
    - These numbers will be tracked on a monthly basis.
    - The conversion rate will be the number of clicks on the call to action such as “Learn more about Nkwashi” within the post
    - Benchmark in 2021-March
    - Target to increase the number by 40% by 2021-December
* The total value of the AIR program per resident will be the total budget of actual and prospective residents.
    - The value at the start of the program will be $10,444 per resident and this value will decrease with the increase of each prospective resident.
    - The prospective resident is assumed to be part of a household and therefore would include an additional adult bringing the residents to 2 when they progress on the construction journey (purchase a floor plan; have their construction approved; start construction)
* Chena will track the following metrics:
    - Number of attendees per event
    - Number of returning attendees
    - Social media followers growth
    - Social media engagement
    - Number of social media mentions


### Communications
#### Synchronous communication
- Scheduled Weekly Check-in meetings on Zoom: access this [Agenda](https://docs.google.com/document/d/1zOyInf-XJg5JnX5HnDOAirKc537eXON1sG7Q9dieTqY/edit#)
- AIR Slack channel to share media and progress
- WhatsApp for real-time communication
#### Asynchronous communication
- Weekly async report due every Friday, to be filled out by Chena on this [document](https://docs.google.com/document/d/1zOyInf-XJg5JnX5HnDOAirKc537eXON1sG7Q9dieTqY/edit?usp=sharing)


### Marketing
#### Social media presence
- The AIR Program can be found on [Instagram](https://www.instagram.com/air.nkwashi/?hl=en)  
- and on [Facebook](https://web.facebook.com/air.nkwashi/?view_public_for=108024391080184)

#### Nkwashi Newsletter
- The AIR Program is featured every Friday on the Nkwashi Newsletter.

### Budget
The AIR Program is mainly funded by DoubleGDP. Chena is to submit a budget prior to each AIR program edition. The budget is to be approved by Nolan, CEO of DoubleGDP. The details of the budget can be found here in the [AIR Budget Spreadsheet](https://drive.google.com/file/d/1Um4WxIBY6ZIxJcDjADvKKD8h9Me_9VVW/view?usp=sharing)

### Administrative Resources
- The AIR 2 signed agreements between DoubleGDP and the artists are available on [this folder](https://drive.google.com/drive/folders/1sk81tnk7D8nWoXArJvmWrtWU-Ahbq_Qh?usp=sharing)
- The AIR 2 signed agreement between Chena and DoubleGDP is available on [this folder](https://drive.google.com/drive/folders/18cmyEMHaDCAewta5I21hFtjhkP9qxCQw?usp=sharing)  
