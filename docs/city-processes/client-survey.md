<h1>Client Survey</h1>

The Client Survey is currently created using Google Forms which is then linked to an article and posted on the News page of the app for clients to fill in and submit.

<h1>Purpose</h1>

The purpose of conducting Client Survey's is to gauge an understanding of;
 
<ul>
<li>How clients use the product</li>
<li>How the product can be improved to better serve the clients needs </li>
<li>What our strengths and weaknesses are and what can be improved</li>
</ul>

<h1>Incentive</h1>

To improve our survey response rate and collect statistically significant data, we use incentives to draw in more responses from clients.

The most common incentive we have provided to date is a Gift Voucher from selected major supermarkets dependant on the city at the time. The incentive is also provided to clients who we conduct interviews with as a thank you for their willingness to share their journey to owning a house.
