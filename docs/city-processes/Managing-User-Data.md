---
title: Managing User Data
---

### Overview

Digital identity is one of core capbilities of the app. All other city processes are dependent on accurate and relevant digital identities in order for the city to work optimally. 

When used correctly user data allows the city to manage payments and other revenues; effectively communicate marketing offers and efficiently manage issues raised by the community members. 

### User Types

City managers can update the following key user types:

1. **Resident:** This user type is given for individuals who live in the city as home-owners; renters or participants of residency programs. They receive specific information such as service disruption updates. Their number is tracked to ensure the city is able to provide responsive services

1. **Client:** This user type is for individuals who have signed up for a plan to own property in the city. This may also apply to clients that have finished the purchase process and are constructing their property. 

1. **Prospective Client:** This user type applies to individuals that are interested in owning property in the city and are yet to start the purchase process. They may receive targted promotional campaigns. 


### Customer Journey Stages

City managers use the customer journey stages to track the progress clients are making towards residency that is a key factor in city growth. 

1. **Plot Fully Purchased:** This applies to clients that have completed the purchase process as per their selected plan. These clients can receive targeted communication on how to start the construction process. 

1. **Floor Plan Purchased:** These clients have selected and paid for their desired floor plan.

1. **Building Permit Approved:** These cilents have met all the requirements to begin construction including plot and floor plan purchased. They would have a project team in place and have received the city's approval to start construction.

1. **Construction in Progress:** These clients have started the construction process and the property is not ready for occupation. 

1. **Construction Completed:** These clients have completed the construction process and the property is ready for occupation. 
