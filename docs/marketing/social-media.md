---
title: Social Media
---

## Accounts
DoubleGDP has the following social media accounts:

* Twitter: [@2xgdp](https://twitter.com/2xgdp)
* Facebook: [DoubleGDP](https://www.facebook.com/doublegdp)
* YouTube: [DoubleGDP](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig)
* LinkedIn: [DoubleGDP](https://www.linkedin.com/company/doublegdp/)

We also have some social media presence through our Artists In Residence program. Those are listed [here](/city-processes/air-program/#social-media-presence). 

## Twitter

### Purpose of building a Twitter presence

* Build a network of people interested in building new cities
* Increase visibility and credibility in the tech scene.
* Leverage already existing connections and affiliations (by following and getting followed by industry leaders).

### General Twitter Guidelines

* Tweet from @2xgdp account
* Keep the tweet concise.
* Limit hashtags to 1-2 per Tweet.
* Include a clear call-to-action where applicable (i.e.” Read the full story here”).
* Have a conversational, informative and optimistic tone. Avoid snarky or cynical tone.
* Monitor events and trending conversations to tweet appropriately.
* Use images, GIFs, and/or videos whenever possible.
* Use emojis to add emotion when it applies.
* Ask questions and run polls (see poll ideas here https://business.twitter.com/en/blog/engaging-twitter-poll-ideas.html).
* Do not express political views.

### Tweet Approval Process

* All tweets should be posted to the Tweet Slack channel for approval.
* Tweets are to be approved by Nolan, Jay or Minza.
* The main account handlers are Zoussi and Justin, although any team member can submit tweets.


### At which frequency should we tweet?

* 1 to 2 times a week, in addition to our bi-weekly sprint updates


### Who should we follow?

* C-level and executives from successful and interesting start-ups.
* Key stakeholders from the start-up scene.
* New Cities / Charter Cities organizations, leaders, and influencers.
* Global Business magazines.
* Media platforms focused on tech, emerging markets and urbanization (i.e. Techpoint Africa, TechCrunch, etc).


### What should we tweet?

* Product updates and app features: #FridayFeature.
* Nkwashi news.
* Media content from events taking place at Nkwashi (i.e. marathon, AIR events, Little Explorers, etc).
* Original blog posts (note: format links with Bitly).
* Blog posts and news articles written by other platforms about: Urbanization in emerging markets, New Cities, and community building.

### About hashtags

* For tweets related to events at Nkwashi, the hashtag #liveatnkwashi should be used.
* For tweets related to news at Nkwashi, the hashtag #Nkwashicity should be used.
* For tweets related to news about New Cities, hashtags related to the post itself can be used (i.e. #appoloniacity #newcities #chartercities).


## Sprint Updates
At the end of each sprint, CEO posts an update to a variety of social media channels. Here is the process:

1. Videos from each teammate are complete by AM on the Wednesday of the sprint end. See [demo process](/prod-eng/productdev/#sprint-demo) for details on this.
1. Confirm that all videos are compiled in a YouTube playlist titled `Progress Updates YYYY-MM-DD`
1. Add CEO video as first video in playlist; reorder the others roughly with the most important / informative updates nearest the top
1. Post the video and 1-2 highlights to Twitter from the @nolanmyers account. Add #newcities and #remotework hashtags.
1. \[EA:\] Using Hootsuite, schedule the same post in our other media channels: including Twitter: @2xdp, Facebook: DoubleGDP, LinkedIn: DoubleGDP, and LinkedIn: nolanmyers. 
