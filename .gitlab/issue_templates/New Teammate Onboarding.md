Welcome to DoubleGDP! This issue tracks TODOs that are required for all new teammates for you and your manager to help welcome you efficiently to the team. Please look for a separate issue that tracks TODOs for new engineers.


## Manager TODOS
- [ ] Confirm Sys Admin created accounts in all of the required systems (see list below)
- [ ] If teammate is non-US based, email [accounting@doublegdp.com](mailto:accounting@doublegdp.com) with the teammate's name, start date, and a reminder to pre-generate the first 12 invoices (if applicable)
- [ ] Schedule a 30-minute all-team "welcome" meeting
- [ ] Schedule a daily checkin with teammate for the first 2 weeks (see [schedule](https://handbook.doublegdp.com/people-group/onboarding/#schedule))
- [ ] Schedule a recurring 1:1 starting in week 3
- [ ] Close out job posting in Greenhouse
- [ ] In Greenhouse, mark candidate as "hired" and link profile to their DoubleGDP email account


## New Teammate TODOS
**Administrative**
- [ ] Ensure your payroll and employment information has been sent to [Accounting](mailto:accounting@doublegdp.com)
- [ ] Accept the invite to all of our productivity tools
- [ ] Set up your personal workspace. See our guidelines for [personal office setup](https://handbook.doublegdp.com/people-group/workstation_setup/)
  - Remember to register any purchases over $500 according to our [Fixed Asset Policy](https://handbook.doublegdp.com//finance/)
  - Register your devices here: [Employee Equipment - Fixed Asset Tracking](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?usp=sharing)
- [ ] Make your first "hello world" commit to the handbook using [this process](https://handbook.doublegdp.com/handbook/)
- [ ] Send a profile picture to Nolan for [our website](https://www.doublegdp.com/about/) and send the URL to your LinkedIn profile.
    - [ ] Select a picture that follows [these guidelines](https://business.linkedin.com/talent-solutions/blog/2014/12/5-tips-for-picking-the-right-linkedin-profile-picture)
    - [ ] Set that picture as your avatar in all of our collaboration tools (GitLab, Slack, GSuite)
- [ ] Update your LinkedIn profile and send a connection request to [your colleagues](https://www.doublegdp.com/about/) (This is a suggestion, not a requirement. Consider using the same picture on our website, for continuity.)
- [ ] Add your birthday (mm-dd) and start date (mm-dd) to our [company milestones](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=0)
    - [ ] Confirm that there is a 'Holiday' tab for your country in this same spreadsheet, and create one if not
    - [ ] Add the holidays you intend to take to your calendar, [using these instructions](https://handbook.doublegdp.com/people-group/#how-to-take-time-off)
- [ ] Confirm that your GitLab notifications are on and that you are able to receive them
- [ ] Add yourself and your role to our [Handbook Teams Page](https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/company/teams.md/)
- [ ] Create an email signature for yourself
- [ ] In slack, browse all available channels and join accordinly to your role.



**Get to know the company**
- [ ] Go through our onboarding flow at [Onboarding](https://handbook.doublegdp.com/people-group/onboarding/)
- [ ] Log into the [DoubleGDP app](https://app.doublegdp.com) using your DGDP Google account and start familiarizing yourself. For bonus points, keep some notes of things that can be improved so you can file a feature suggestion!
- [ ] Read one article from our [knowledge base](https://www.doublegdp.com/progress/05-what-we-are-learning/) and share an observation with your onboarding team
- [ ] Read one entry from our [blog](https://www.doublegdp.com/blog/) and share an observation with your onboarding team


## System Administrator TODOs
For the system administrator to complete / teammate to confirm. (Manager: please assign to either Nolan or Cecilia.)
- [ ] Google Suite, and any group membership within (e.g. "eng" or "csm")
- [ ] GitLab - use "Maintainer" role under the "DoubleGDP" Group by default
- [ ] Slack
- [ ] Pilot
- [ ] [YouTube](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig) - use "Manager" permission level
- [ ] [OnePassword](https://doublegdp.1password.com/)
    - [ ] Add to vaults (after invite is accepted)
- [ ] [Zoom](https://zoom.us) (if appliable) - use "Basic" account for engineers, "Licensed" account for teammates who we expect to host meetings frequently
    - [ ] [Connect your Zoom account with your DGDP Calendar](https://support.zoom.us/hc/en-us/articles/360020187492-Google-Calendar-add-on)
- [ ] [Expensify](https://www.expensify.com/domain_members) (if applicable)
- [ ] [AirTable CRM](https://airtable.com/invite/l?inviteId=invZ8AGEsyzttooGu&inviteToken=9a51ce3c8881f4d03e07707654a7cead06c16e061133db859934848b515f20fe) (if applicable)
- [ ] [Calendly](https://calendly.com), if applicable
- [ ] [Pto by Roots]


### Reference: Additional setup for Engineers
- [ ] Setup Development Environment
    - [ ] Digital Ocean
    - [ ] Google SSO
    - [ ] Facebook SSO
    - [ ] Certficate SSL
    - [ ] CloudflareDNS
- [ ] Invite to Rollbar
- [ ] Go over Engineering-specific values and expectations
