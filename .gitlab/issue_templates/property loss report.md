If you've the loss of property that may have access to company or customer data (e.g. a laptop or phone), it's important to report and resolve the incident quickly. Here is the procedure, with tasks for the "Teammate" whose property has been lost, and the "Admin" with access to the systems to revoke it. (This Admin may be the teammate's manager or a Business Operations Manager.)

- [ ] [Teammate] Email [all@doublegdp.com](mailto:all@doublegdp.com) letting us know what's been taken
- [ ] [Admin] On Google Account, reset password, clear sign-in cookies, and remove all connected applications
- [ ] [Admin] If engineer, reset SSL keys and dev environment at digitalocean
- [ ] [Admin] if engineer, revoke PagerDuty and Rollbar access.
- [ ] [Admin] if engineer, revoke access to 1Password7
- [ ] [Admin] if engineer, revoke Gitlab access.
- [ ] [Admin] Review list of productivity tools to see if there are any other apps to reset
- [ ] [Admin] if engineer, revoke access in Heroku
- [ ] [Admin] Revoke access to any community administrator accounts


